#!/bin/bash

#singly cycles
for SIZE in {50..5000..50}
    do
        TIME=`awk -v S=$SIZE '$1==S {print $2}' $1`
        # awk -v S=$SIZE '{}'
        TRIALSPERBATCH=`awk -v T=$TIME 'BEGIN {print int(1200/T)}'`
    
        TOTALTRIALS=`awk -v S=$SIZE 'BEGIN {print 2000000/S}'`

        TOTALTRIALS_ROUNDED=`awk -v R=$TOTALTRIALS 'BEGIN {printf("%.0f", R)}'`
        
        NUMBERJOBS=`awk -v T=$TOTALTRIALS_ROUNDED -v B=$TRIALSPERBATCH 'BEGIN {print int(T/B)}'`
        echo $NUMBERJOBS
        STRAYJOBS=`awk -v T=$TOTALTRIALS_ROUNDED -v B=$TRIALSPERBATCH 'BEGIN {print T%B}'`
        echo $STRAYJOBS
    done

#doubly cycles
for SIZE in {50..5000..50}
    do
        TIME=`awk -v S=$SIZE '$1==S {print $2}' $2`
        # awk -v S=$SIZE '{}'
        TRIALSPERBATCH=`awk -v T=$TIME 'BEGIN {print int(1200/T)}'`
    
        TOTALTRIALS=`awk -v S=$SIZE 'BEGIN {print 2000000/S}'`

        TOTALTRIALS_ROUNDED=`awk -v R=$TOTALTRIALS 'BEGIN {printf("%.0f", R)}'`
        
        NUMBERJOBS=`awk -v T=$TOTALTRIALS_ROUNDED -v B=$TRIALSPERBATCH 'BEGIN {print int(T/B)}'`
        echo $NUMBERJOBS
        STRAYJOBS=`awk -v T=$TOTALTRIALS_ROUNDED -v B=$TRIALSPERBATCH 'BEGIN {print T%B}'`
        echo $STRAYJOBS
    done
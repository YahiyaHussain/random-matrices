#!/bin/bash

#singly cycles
make -f main_Makefile
for SIZE in {1000..4000..1000}
    do
        echo "Singly: $SIZE"
		#echo "Calculating Singly $SIZE Data"
        TIME=`awk -v S=$SIZE '$1 == S {print $2} ' $1`
        #echo $TIME
        MAXTRIALSPERJOB=`awk -v T=$TIME 'BEGIN {print int(1200/T)}'`
        #echo $TRIALSPERBATCH
        TRIALSTODO=`awk -v S=$SIZE 'BEGIN {print 2000000/S}'`
        #echo $TRIALSTODO
        TRIALSTODO_ROUNDED=`awk -v R=$TRIALSTODO 'BEGIN {printf("%.0f", R)}'`
        #echo $TRIALSTODO_ROUNDED
        NUMBERJOBS=`awk -v T=$TRIALSTODO_ROUNDED -v M=$MAXTRIALSPERJOB 'BEGIN {print int(T/M)}'`
        #echo $NUMBERJOBS

        #echo $NUMBERJOBS
        # STRAYJOBS=`awk -v T=$TRIALSTODO_ROUNDED -v B=$TRIALSPERBATCH 'BEGIN {print T%B}'`
        # echo $STRAYJOBS


        #mkdir $SIZE.singly
		#cp *.exec $SIZE.singly
        #cp 20MinBatchEigen_BSub.sh $SIZE.singly

        if [ $NUMBERJOBS -gt 0 ]
            then
                echo "size $SIZE" > $SIZE.pf
                echo "cycle singly" >> $SIZE.pf
                echo "trials $MAXTRIALSPERJOB" >> $SIZE.pf

                #echo "Executing Singly $SIZE"
                #mv $SIZE.pf $SIZE.singly
                #cd $SIZE.singly
                ./20MinBatchEigen_BSub.sh $SIZE $NUMBERJOBS s
                #cd ..
            fi

        STRAYJOBS=`awk -v T=$TRIALSTODO_ROUNDED -v M=$MAXTRIALSPERJOB 'BEGIN {print T%M}'`
        echo "size $SIZE" > $SIZE.stray.pf
        echo "cycle singly" >>  $SIZE.stray.pf
        echo "trials $STRAYJOBS" >> $SIZE.stray.pf

		#mv $SIZE.stray.pf $SIZE.singly
		#cd $SIZE.singly
		./20MinBatchEigen_BSub.sh $SIZE stray s
		#cd ..

    done

#doubly cycles
for SIZE in {1000..4000..1000}
    do
        echo "Doubly: $SIZE"
	#echo "Calculating Doubly $SIZE Data"
        #echo "Calculating Singly $SIZE Data"
        TIME=`awk -v S=$SIZE '$1 == S {print $2} ' $2`
        #echo $TIME
        MAXTRIALSPERJOB=`awk -v T=$TIME 'BEGIN {print int(1200/T)}'`
        #echo $TRIALSPERBATCH
        TRIALSTODO=`awk -v S=$SIZE 'BEGIN {print 2000000/S}'`
        #echo $TRIALSTODO
        TRIALSTODO_ROUNDED=`awk -v R=$TRIALSTODO 'BEGIN {printf("%.0f", R)}'`
        #echo $TRIALSTODO_ROUNDED
        NUMBERJOBS=`awk -v T=$TRIALSTODO_ROUNDED -v M=$MAXTRIALSPERJOB 'BEGIN {print int(T/M)}'`


        # mkdir $SIZE.doubly
		# cp *.exec $SIZE.doubly
        # cp 20MinBatchEigen_BSub.sh $SIZE.doubly

        if [ $NUMBERJOBS -gt 0 ]
            then
                echo "size $SIZE" > $SIZE.pf
                echo "cycle doubly" >> $SIZE.pf
                echo "trials $MAXTRIALSPERJOB" >> $SIZE.pf

                #echo "Executing Singly $SIZE"
                #mv $SIZE.pf $SIZE.doubly
                #cd $SIZE.doubly
                ./20MinBatchEigen_BSub.sh $SIZE $NUMBERJOBS d
                #cd ..
            fi

        STRAYJOBS=`awk -v T=$TRIALSTODO_ROUNDED -v M=$MAXTRIALSPERJOB 'BEGIN {print T%M}'`
        echo "size $SIZE" > $SIZE.stray.pf
        echo "cycle doubly" >>  $SIZE.stray.pf
        echo "trials $STRAYJOBS" >> $SIZE.stray.pf

		#mv $SIZE.stray.pf $SIZE.doubly
		#cd $SIZE.doubly
		./20MinBatchEigen_BSub.sh $SIZE stray d
		#cd ..

    done

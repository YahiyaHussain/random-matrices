#!/bin/bash



for SIZE in {1200..1550..50}
do
	NEXTSIZE=$(($SIZE + 50))
	echo "$SIZE `./histogramDistance.exec all.$SIZE.norm all.$NEXTSIZE.norm`" >> 4MillDist.dat
done

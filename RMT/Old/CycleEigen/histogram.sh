#!/bin/bash

for SIZE in {50..4950..50}
do
    echo all.$SIZE.$1.eigen
    ./histogram.exec -45 1 400 all.$SIZE.$1.eigen > temp.hist
    ./normalize.exec `wc -l < all.$SIZE.$1.eigen` 1 temp.hist > all.$SIZE.$1.norm
done
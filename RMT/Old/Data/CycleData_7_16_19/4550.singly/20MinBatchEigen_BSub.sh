#!/bin/bash

if [ $2 == "stray" ]
then
    bsub \
        -J $3.$1.stray \
        -q short -W 01:00 \
        -R rusage[mem=512] \
        -R span[hosts=1]  \
        -oo $1.stray.log \
        -eo $1.stray.error \
        -n 1 \
        ./eigen.exec $1.stray.pf $1.stray.eigen
    exit 1
fi

for (( c=1; c<=$2; c++ ))
    do
        #mkdir "./eigen.exec $1.pf $1.$c.eigen"
        bsub \
        -J $3.$1.$c \
        -q short -W 01:00 \
        -R rusage[mem=512] \
        -R span[hosts=1]  \
        -oo $1.$c.log \
        -eo $1.$c.error \
        -n 1 \
        ./eigen.exec $1.pf $1.$c.eigen
    done

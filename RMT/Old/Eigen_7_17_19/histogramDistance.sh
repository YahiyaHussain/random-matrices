#!/bin/bash

for SIZE in {50..4900..50}
do
    NEXTSIZE=$(($SIZE + 50))
    ./histogramDistance.exec all.$SIZE.singly.norm all.$NEXTSIZE.singly.norm singlyDistance.txt
    #./histogramDistance.exec all.$SIZE.doubly.norm all.$NEXTSIZE.doubly.norm doublyDistance.txt
    
done
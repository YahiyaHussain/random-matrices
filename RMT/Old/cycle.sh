#!/bin/bash

clear
clear

make
echo "Doubly Cycles"
./eigen Double 5
./eigen Double 10
./eigen Double 100
./eigen Double 500
./eigen Double 2000
./eigen Double 3000
 
echo "Singly Cycles"
./eigen Single 5
./eigen Single 10
./eigen Single 100
./eigen Single 500
./eigen Single 2000
./eigen Single 3000

./plot.sh
#!/bin/bash

##1000
for j in {1..10}
do
    bsub \
        -J 1000 \
        -q short -W 04:00 \
        -R rusage[mem=2048] \
        -R span[hosts=1]  \
        -oo 1000.$j.g.log \
        -eo 1000.$j.g.error \
        -n 1 \
        python3 doublycycle.py 1000 200 1000.d.$j.eigen
done


##2000
for j in {1..50}
do
    bsub \
        -J 2000 \
        -q short -W 04:00 \
        -R rusage[mem=2048] \
        -R span[hosts=1]  \
        -oo 2000.$j.g.log \
        -eo 2000.$j.g.error \
        -n 1 \
        python3 doublycycle.py 2000 20 2000.d.$j.eigen
done

##3000
for j in {1..96}
do
    bsub \
        -J 3000 \
        -q short -W 04:00 \
        -R rusage[mem=2048] \
        -R span[hosts=1]  \
        -oo 3000.$j.g.log \
        -eo 3000.$j.g.error \
        -n 1 \
        python3 doublycycle.py 3000 7 3000.d.$j.eigen
done

##4000
for j in {1..100}
do
    bsub \
        -J 4000 \
        -q short -W 04:00 \
        -R rusage[mem=2048] \
        -R span[hosts=1]  \
        -oo 4000.$j.g.log \
        -eo 4000.$j.g.error \
        -n 1 \
        python3 doublycycle.py 4000 5 4000.d.$j.eigen
done

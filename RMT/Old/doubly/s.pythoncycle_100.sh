#!/bin/bash

##100
for j in {1..10}
do
    bsub \
        -J 1000 \
        -q short -W 04:00 \
        -R rusage[mem=2048] \
        -R span[hosts=1]  \
        -oo 100.$j.g.log \
        -eo 100.$j.g.error \
        -n 1 \
        python3 singlycycle.py 100 2000 100.s.$j.eigen
done

##200
for j in {1..10}
do
    bsub \
	-J 1000 \
	-q short -W 04:00 \
	-R rusage[mem=2048] \
	-R span[hosts=1] \
	-oo 200.$j.g.log \
	-eo 200.$j.g.log \
	-n 1 \
	python3 singlycycle.py 200 1000 200.s.$j.eigen
done

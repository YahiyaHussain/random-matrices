import numpy as np
import sys
import gc
import time

#gc.disable
file = open(sys.argv[3]+".log", 'w')
file.write('')
file.close

file = open(sys.argv[3]+".imag", 'w')
file.write('')
file.close

file = open(sys.argv[3]+".real", 'w')
file.write('')
file.close

for i in range(int(sys.argv[2])):
    A = np.asfortranarray(np.random.exponential(1, int(sys.argv[1]) - 1))
    M = np.asfortranarray(np.diagflat(A, -1))
    M[0, -1] = np.random.exponential(1)
    M = M - np.diag(np.sum(M, axis=0))
    E = np.linalg.eigvals(M)
    L = np.log(np.absolute(E))
    #s = np.savetxt(sys.stdout.buffer, E, fmt='%.8f')

    with open(sys.argv[3] + ".log", 'a') as file:
        for i in range(0, int(sys.argv[1])):
            file.write(str(L[i]) + '\n')
    with open(sys.argv[3]+".imag", 'a') as file:
        for i in range(0, int(sys.argv[1])):
            file.write(str(np.imag(E[i]))+'\n')
    with open(sys.argv[3]+".real", 'a') as file:
        for i in range(0, int(sys.argv[1])):
            file.write(str(np.real(E[i]))+'\n')
#sys.stdout.write(str(s))

#!/bin/bash

if [ $# -gt 0 ]
    then
        cd $1
        for D in *
            do
                if [ -d "${D}" ]
                    then
                        cd $D
                        rm $D.all.eigen
                        cat *.eigen > $D.all.eigen
                        cp $D.all.eigen ../
                        rm $D.all.eigen
                        cd ..
                fi

            done
        cp *.eigen ../
        rm *.eigen
        cd ..
        exit 1
    fi

echo "ERROR NO FILE NAME GIVEN"
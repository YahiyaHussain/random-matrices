

singlyhistograms = zeros(5, 500);
singlyBins = zeros(5, 500);
doublyhistograms = zeros(5, 500);
doublyBins = zeros(5, 500);
for i=1:5
    matrixSize = 1000*i;
    Trials = 2e6/matrixSize;
    result = zeros(matrixSize, round(Trials));
    tic
    
    
    
     parfor(t = 1:round(Trials), 20)
         M = exprnd(1, [matrixSize - 1, 1]);
         
         
         M = diag(M, -1);
         M(1, end) = exprnd(1);
         
         M = M - diag(sum(M));
         result(:, t) = eig(M);
     end
    toc
    
    [Pr_eig, Bins_eig] = hist(log(abs(result(:))), 500);
    Pr_eig = Pr_eig/sum(Pr_eig);
    singlyBins(i, :) = Bins_eig;
    singlyhistograms(i, :) = Pr_eig;
end

for i=1:5
    matrixSize = 1000*i;
    Trials = 2e6/matrixSize;
    result = zeros(matrixSize, round(Trials));
    tic
    
    parfor(t = 1:round(Trials), 20)
        M = exprnd(1, [matrixSize - 1, 1]);
        
        M = diag(M, -1);
        M(1, end) = exprnd(1);
        
        B = exprnd(1, [matrixSize - 1, 1]);
        
        M = M + diag(B, 1);
        M(end, 1) = exprnd(1);
        
        M = M - diag(sum(M));
        result(:, t) = eig(M);
    end
    toc
    
    [Pr_eig, Bins_eig] = hist(log(abs(result(:))), 500);
    Pr_eig = Pr_eig/sum(Pr_eig);
    doublyBins(i, :) = Bins_eig;
    doublyhistograms(i, :) = Pr_eig;
end

% save('Cycle_Spread_Data.mat','singlyhistograms', 'singlyBins', ;
% doublyhistograms = zeros(5, 500);
% doublyBins = zeros(5, 500););

function Polish_Figure

ax = gca; 
outerpos = ax.OuterPosition;
ti = ax.TightInset;
left = outerpos(1) + ti(1)+.01;
ax_width = outerpos(3) - ti(1)- ti(3)-.02;
% left = outerpos(1) + ti(1)-.1;
% ax_width = outerpos(3) - ti(1)- ti(3)-.02;
bottom = outerpos(2) + ti(2)+.01;
ax_height = outerpos(4) - ti(2) - ti(4)-.02;
ax.Position = [left bottom ax_width ax_height];

F = gcf;   F.Color = 'white'
F.PaperPosition = [0,0,8,6];
set(gcf,'InvertHardcopy','Off','PaperPositionMode','auto');
fig_pos = F.PaperPosition;
F.PaperSize = [fig_pos(3) fig_pos(4)];
Ma%parameter goes from 11 to 40 +11
deltasize = 25;
startsize = 1
%I am graphing the distances between 10 graphs with 500 bins each, a matrix
save

%Go from parameter size start to delta+start
for s=startsize:deltasize+startsize
    %multiply 2000 by parameter for matrix size so matrices go from
    %200 to 5200
    matrixSize = s*200;
    
    %approximate 2e6 eigenvalues with number of matrix trials
    Trials = 2e6/matrixSize;
    result = zeros(matrixSize, round(Trials));
    tic
    
    %make a cycle matrix and get eigen
    parfor (t=1:round(Trials), 20)
        %make column of exponential sampled values
        M = exprnd(1, [matrixSize - 1, 1]);
        %make into matrix with those values on the diagonal except each
        %shifted down by 1
        M = diag(M, -1);
        %add in the entry first to last
        M(1, end) = exprnd(1);
        
        %---doubly cycles only
        B = exprnd(1, [matrixSize - 1, 1]);
        %add exponentially sampled values to the diagonal
        %shifted up by 1
        M = M + diag(B, 1);
        %add in the entry last to first
        M(end, 1) = exprnd(1);
        %---------------------
        
        %make this a rate matrix by putting the
        %negative column sums on the diagonal
        M = M - diag(sum(M));
        %add eigen to an array of eigen
        result(:, t) = eig(M);
    end
    toc
    
    %histogram all the eigen with a log scale
    [Pr_eig, Bins_eig] = hist(log(abs(result(:))), 500);
    Pr_eig = Pr_eig/sum(Pr_eig);
    %add histogram to a list of histograms
    histograms(s, :) = Pr_eig;
end

%calculate the distances between consecutive (200 spaced) histograms
sizes = zeros(deltasize - 1, 1);
distances = zeros(deltasize -1, 1); 
for s=1:deltasize -1
    %make the x axis, have the size of the matrix
    sizes(s) = (startsize + s) * 200;
    d = histograms(s) - histograms(s+1);
    %calculate the norm of the difference of size and size+1 hists
    %and add to a list of distances
    distances(s) = norm(d);
end
%save for later
save("testConvergence_Doubly");
%plot with good formatting
figure(001), plot(sizes, distances, '-*', 'linewidth', 3)
ax = gca;
ax.FontSize = 18;
%what font does rebecca use
xlabel('$N$','interpreter','latex','fontsize',23)
ylabel('$\parallel \rho(k+1)-\rho(k)\parallel_2$','interpreter','latex','fontsize',23)


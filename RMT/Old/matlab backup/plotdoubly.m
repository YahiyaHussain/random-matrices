load("Cycle_Spread_Data.mat");

hold on
for i=1:5
figure(i), plot(singlyBins(i, :), singlyhistograms(i,:), '-*', 'linewidth', 3)
ax = gca;
ax.FontSize = 18;
%what font does rebecca use
xlabel('$N$','interpreter','latex','fontsize',23)
ylabel('$\parallel \rho(k+1)-\rho(k)\parallel_2$','interpreter','latex','fontsize',23)
end
hold off
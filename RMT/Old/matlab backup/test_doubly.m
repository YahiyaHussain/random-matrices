

size = 5000;
A = exprnd(1, [size - 1, 1]);
B = exprnd(1, [size - 1, 1]);
M = diag(A, -1) + diag(B, 1);
M(1, end) = exprnd(1);
M(end, 1) = exprnd(1);

M = M - diag(sum(M));
M = sparse(M);
tic
eigs(M, size)
toc
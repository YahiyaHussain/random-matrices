#!/bin/bash
for BATCH in {1..$1..1}
do
	mkdir $1.$BATCH.eigen
done

if [ $2 -gt 0 ]
then
	mkdir $0.stray.eigen
fi

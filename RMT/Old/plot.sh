#!/bin/bash


./scale `"1.0 / (($1)^0.5)" | bc` $1.all.eigen > $1.all.eigen.scaled 
./histogram -45 1 500 $1.all.eigen.scaled > $1.all.hist
./normalize 1 `wc -l < $1.all.eigen` $1.all.hist > $1.all.norm


gnuplot -e "set term png size 400,300; set output '$1-SinglyCycle.png' ;plot '$1.all.norm' u 1:3 w l"






#!/bin/bash

for SIZE in {1250..1600..50}
do
	./histogram.exec -45 1 300 all.$SIZE.eigen > all.$SIZE.hist
	./normalize.exec 1 `wc -l < all.$SIZE.eigen` all.$SIZE.hist > all.$SIZE.norm
done

#!/bin/bash

for SIZE in {50..5000..50}
do
	echo "size $SIZE" > $SIZE.pf
	echo "cycle singly" >> $SIZE.pf
	echo "eigen 2000000" >> $SIZE.pf

	echo ./eigen.exec $SIZE.pf $SIZE.eigen
	time ./eigen.exec $SIZE.pf $SIZE.eigen
done

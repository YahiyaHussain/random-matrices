#!/bin/bash

TIME=`awk -v S=$2 '$1 == S {print $2} ' $1`
echo time: $TIME
TRIALSPERBATCH=`awk -v T=$TIME 'BEGIN {print int(1200/T)}'`
echo trials per batch: $TRIALSPERBATCH
MINIMUMTRIALS=`awk -v S=$2 'BEGIN {print 2000000/S}'`
echo minimumtrials: $MINIMUMTRIALS
MINIMUMTRIALS_ROUNDED=`awk -v R=$MINIMUMTRIALS 'BEGIN {printf("%.0f", R)}'`
echo minimum trials rounded: $MINIMUMTRIALS_ROUNDED
NUMBERJOBS=`awk -v T=$MINIMUMTRIALS_ROUNDED -v B=$TRIALSPERBATCH 'BEGIN {print int(T/B)}'`
echo number of jobs: $NUMBERJOBS

TIME=`awk -v S=$SIZE -v '$1 == S {print $2} ' $2`
        echo $TIME
        MAXTRIALSPERJOB=`awk -v T=$TIME 'BEGIN {print int(1200/T)}'`
        #echo $TRIALSPERBATCH
        TRIALSTODO=`awk -v S=$SIZE 'BEGIN {print 2000000/S}'`
        #echo $TRIALSTODO
        TRIALSTODO_ROUNDED=`awk -v R=$TRIALSTODO 'BEGIN {printf("%.0f", R)}'`
        #echo $TRIALSTODO_ROUNDED
        NUMBERJOBS=`awk -v T=$TRIALSTODO_ROUNDED -v M=$MAXTRIALSPERJOB 'BEGIN {print int(T/M)}'`
        echo $NUMBERJOBS
import numpy as np

M = np.random.normal(0, 1, (2000, 2000))

E = np.linalg.eigvals(M)

R = E.real
I = E.imag

with open("test.r", 'a') as file:
   for i in range(1, 2000):
       file.write(str(R[i]) + '\n')
with open("test.i", 'a') as file:
   for i in range(1, 2000):
       file.write(str(I[i]) + '\n')

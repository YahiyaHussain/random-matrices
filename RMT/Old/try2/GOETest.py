import numpy as np
import sys
import gc
import time

#gc.disable
file = open(sys.argv[3], 'w')
file.write('')
file.close

for i in range(int(sys.argv[2])):
    A = np.asfortranarray(np.random.normal(size = int(sys.argv[1]) - 1))
    M = np.asfortranarray(np.diagflat(A, -1))
    M[0, -1] = np.random.normal()

    B = np.asfortranarray(np.random.normal(size = int(sys.argv[1]) - 1))
    M = M + np.diagflat(B, 1)
    M[-1, 0] = np.random.normal()


    #M = M - np.diag(np.sum(M, axis=0))
    #E = np.log( np.absolute(np.linalg.eigvals(M)))
    E = np.linalg.eigvals(M)
    #s = np.savetxt(sys.stdout.buffer, E, fmt='%.8f')


    R = E.real
    I = E.imag
    with open(sys.argv[3]+".r", 'a') as file:
        for i in range(1, int(sys.argv[1])):
            file.write(str(R[i]) + '\n')

    with open(sys.argv[3]+".i", 'a') as file:
        for i in range(1, int(sys.argv[1])):
            file.write(str(I[i]) + '\n')

#sys.stdout.write(str(s))

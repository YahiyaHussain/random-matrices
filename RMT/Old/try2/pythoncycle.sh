#!/bin/bash

##1000
for j in {1..10}
do
    bsub \
        -J $3.$1.stray \
        -q short -W 01:00 \
        -R rusage[mem=2048] \
        -R span[hosts=1]  \
        -oo $1.stray.log \
        -eo $1.stray.error \
        -n 1 \
        python3 GOETest.py 1000 200 1000.d.$j.eigen
done


##2000
for j in {1..20}
do
    bsub \
        -J $3.$1.stray \
        -q short -W 01:00 \
        -R rusage[mem=2048] \
        -R span[hosts=1]  \
        -oo $1.stray.log \
        -eo $1.stray.error \
        -n 1 \
        python3 GOETest.py 2000 50 2000.d.$j.eigen
done

##3000
for j in {1..30}
do
    bsub \
        -J $3.$1.stray \
        -q short -W 01:00 \
        -R rusage[mem=2048] \
        -R span[hosts=1]  \
        -oo $1.stray.log \
        -eo $1.stray.error \
        -n 1 \
        python3 GOETest.py 3000 23 3000.d.$j.eigen
done

##4000
for j in {1..40}
do
    bsub \
        -J $3.$1.stray \
        -q short -W 01:00 \
        -R rusage[mem=2048] \
        -R span[hosts=1]  \
        -oo $1.stray.log \
        -eo $1.stray.error \
        -n 1 \
        python3 GOETest.py 4000 13 4000.d.$j.eigen
done

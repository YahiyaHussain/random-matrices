#ifndef MY_STACK_H
#define MY_STACK_H

struct StackNode 
{ 
    double data; 
    struct StackNode* next; 
}; 
  
struct StackNode* newNode(double data) 
{ 
    struct StackNode* stackNode = 
              (struct StackNode*) malloc(sizeof(struct StackNode)); 
    stackNode->data = data; 
    stackNode->next = NULL; 
    return stackNode; 
} 

int isEmpty(struct StackNode *root);
  
void push(struct StackNode** root, int data);
  
int pop(struct StackNode** root);
  
int peek(struct StackNode* root);
#endif
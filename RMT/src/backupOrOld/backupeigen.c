#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_blas.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_statistics_double.h>
#include <sys/stat.h>

#include "cycle.h"
#include "variance.h"
#include "build.h"
#include "print.h"
#include "structs.h"
//_________________________________________structs________________________________


/*_____________________________________HELPER FUNCTIONS________________________\
sets up unique rng system based on current time and # trial*/
gsl_rng * setupRNG(int nTrial){
	gsl_rng * r;
	const gsl_rng_type * T;
	gsl_rng_env_setup();
	T = gsl_rng_default;
	r = gsl_rng_alloc(T);
	gsl_rng_set(r, time(0) * nTrial);
	return r;
}
// averages a matrix with its transpose
int comparedoubles(const void * a, const void * b){
	const double fa = * (const double *) a;
	const double fb = * (const double *) b;

	return (fa > fb) - (fb > fa);
}
int comparedoublesByMagnitude(const void *a, const void *b) {
	const double *fa = (const double *)a;
	const double *fb = (const double *)b;
	return ((fabs(*fa) > fabs(*fb)) - (fabs(*fa) < fabs(*fb)));
}
gsl_matrix * make_RealMatrix_ColumnRateMatrix(gsl_matrix * M, int matrixSize){
	double sum;
	for (int j = 0; j < matrixSize; j++){
		sum = 0;
		for (int i = 0; i < matrixSize; i++){
			if (i != j)
				sum += gsl_matrix_get(M, i, j);
		}
		gsl_matrix_set(M, j, j, -sum);
	}

	return M;
}
/*_____________________________________________________________________________|


//_____________________________________EIGEN FUNCTIONS_________________________\
//keep in mind lower triangular and diagonal of matrix are destroyed
//for some reason GSL eigen systems only detect views as square matrices*/
gsl_vector * realSymmEigen(gsl_matrix * M, int n){
	gsl_matrix_view view = gsl_matrix_view_array(M->data, n,n);
	gsl_vector * eval = gsl_vector_alloc(n);
	gsl_eigen_symm_workspace * w = gsl_eigen_symm_alloc(n);
	gsl_eigen_symm(&view.matrix, eval, w);
	gsl_eigen_symm_free(w);
	return eval;
}
gsl_vector * hermEigen(gsl_matrix_complex * M, int n){
	gsl_matrix_complex_view view = gsl_matrix_complex_view_array(M->data,n,n);
	gsl_vector * eval = gsl_vector_alloc(n);
	gsl_eigen_herm_workspace * w = gsl_eigen_herm_alloc(n);
	gsl_eigen_herm(&view.matrix,eval,w);
	gsl_eigen_herm_free(w);
	return eval;
}
gsl_vector_complex * realNonSymmEigen(gsl_matrix * M, int n){
	gsl_matrix_view view = gsl_matrix_view_array(M->data, n, n);
	gsl_vector_complex * eval = gsl_vector_complex_alloc(n);
	gsl_eigen_nonsymm_workspace * w = gsl_eigen_nonsymm_alloc(n);
	gsl_eigen_nonsymm(&view.matrix, eval, w);
	gsl_eigen_nonsymm_free(w);
	return eval;
}
//_____________________________________________________________________________|





//________________________________________import matrix functions_______________

// expects the file to have "i j" separated by newlines for coordinates of matrix
gsl_matrix * import_RealMatrix_FromCoordFile_Exponential(int matrixSize, char * fileName, gsl_rng * r){

	int n_a = -1;
	gsl_matrix * M = build_Zero(n_a, matrixSize, NULL, n_a);
	int iIndex;
	int jIndex;
	double value;

	char * line = NULL;
	size_t len = 0;
	ssize_t read;

	FILE * fp = fopen(fileName, "r");
	if (fp == NULL){
		printf("Could Not Open File");
		gsl_matrix_free(M);
		return NULL;
	}
	else{
		while((read = getline(&line, &len, fp)) != -1){
			char * token = strtok(line, " ");
			iIndex = atoi(token);

			token = strtok(NULL, " ");
			jIndex = atoi(token);

			value = sample_Exponential(r, 1, 1);

			gsl_matrix_set(M, iIndex, jIndex, value);
		}
	}
	fclose(fp);
	if (line){
		free(line);
	}
	return M;
}
gsl_matrix * import_RealMatrix_FromCoordArrays_Exponential(int matrixSize, int arraysSize, int * iIndices, int * jIndices, gsl_rng * r){
	int n_a = -1;
	gsl_matrix * M = build_Zero(n_a, matrixSize, NULL, n_a);
	
	for (int k = 0; k < arraysSize; k++)
	{
		gsl_matrix_set(M, iIndices[k], jIndices[k], sample_Exponential(r, 1, 1));
	}
	return M;
}
// expects the file to have "i j v" separated by newlines for coordinates and values of matrix
gsl_matrix * import_RealMatrix_FromTripleFile(int matrixSize, char * fileName){
	gsl_matrix * Z = build_Zero(-1, matrixSize, NULL, -1);

	int iIndex;
	int jIndex;
	double value;

	char * line = NULL;
	size_t len = 0;
	ssize_t read;

	FILE * fp = fopen(fileName, "r");
	if (fp == NULL){
		printf("Could Not Open File");
		gsl_matrix_free(Z);
		return NULL;
	}
	else{
		while((read = getline(&line, &len, fp)) != -1){
			char * token = strtok(line, " ");
			iIndex = atoi(token);

			token = strtok(NULL, " ");
			jIndex = atoi(token);
			token = strtok(NULL, "\n");
			value = atof(token);

			gsl_matrix_set(Z, iIndex, jIndex, value);
		}
		
	}
	fclose(fp);
	if(line){
		free(line);
	}
	return Z;
}

gsl_matrix * convert_TripleArrays_ToRealMatrix(int matrixSize, int arraysLength, int * iIndexArray, int * jIndexArray, double * valueArray){
	gsl_matrix * Z = build_Zero(-1, matrixSize, NULL, -1);

	for(int k = 0; k < arraysLength; k++){
		gsl_matrix_set(Z, iIndexArray[k], jIndexArray[k], valueArray[k]);
	}
	return Z;
}

//expects arrays are allocated sufficiently and will return size of filled parts of array
int convert_CoordFile_ToCoordArrays(int matrixSize, int * iIndices, int * jIndices, char * fileName){
	
	char * line = NULL;
	size_t len = 0;
	ssize_t read;
	int coordArraysSize = 0;

	FILE * fp = fopen(fileName, "r");
	if (fp == NULL){
		printf("Could Not Open File");
		free(iIndices);
		free(jIndices);
		return -1;
	}
	else{
		while((read = getline(&line, &len, fp)) != -1){
			char * token = strtok(line, " ");
			iIndices[coordArraysSize] = atoi(token);

			token = strtok(NULL, " ");
			jIndices[coordArraysSize++] = atoi(token);

		}
	}
	fclose(fp);
	if (line){
		free(line);
	}
	return coordArraysSize - 1;
}
// expects arrays are sufficiently allocated
int convert_RealMatrix_ToTripleArrays(int matrixSize, int * iIndices, int * jIndices, double * values, gsl_matrix * M){
	int numberElements = 0;
	
	for (int i = 0; i < matrixSize; i++)
	{
		for (int j = 0; j < matrixSize; j++)
		{

			if (gsl_matrix_get(M, i, j) != 0){
				numberElements++;
			}
		}
		
	}

	int kIndex = 0;
	for (int i = 0; i < matrixSize; i++)
	{
		for (int j = 0; j < matrixSize; j++)
		{

			if (gsl_matrix_get(M, i, j) != 0){
				iIndices[kIndex] = i;
				jIndices[kIndex] = j;
				values[kIndex++] = gsl_matrix_get(M, i, j);
			}
		}
		
	}
	return 0;
}
//_____________________________________________________________________________/

//_________________________________matrix exporting functions__________________


int export_RealMatrix_ToTripleFile(int matrixSize, char * fileName, gsl_matrix * M){
	FILE * fp;

	fp = fopen(fileName, "w+");

	for(int i = 0; i < matrixSize; i++)
	{
		for(int j = 0; j < matrixSize; j++)
		{
			fprintf(fp, "%d %d %f\n", i, j, gsl_matrix_get(M, i, j));
		}
		
	}

	fclose(fp);
	return 0;
}
// only keepsxported file "i j "
int export_RealMatrix_ToCoordFile(int matrixSize, char * fileName, gsl_matrix * M){
	FILE * fp;

	fp = fopen(fileName, "w+");

	for(int i = 0; i < matrixSize; i++)
	{
		for(int j = 0; j < matrixSize; j++)
		{
			fprintf(fp, "%d %d\n", i, j);
		}
		
	}

	fclose(fp);
	return 0;
}
//____________________________________________________________________________________/
//_____________________Test Example Functions___________________________________
gsl_matrix * test_ContinMasterEquation_ESRE(double n_a1, int n, gsl_rng * r, double n_a){
	//gsl_rng * r = setupRNG(1);
	
	int iIndices[7] = {0, 3, 3, 0, 2, 5, 1};
	int jIndices[7] = {1, 0, 4, 2, 3, 4, 5};

	double values[7];
	for(int k = 0; k < 7; k++){
		values[k] = sample_Exponential(r, 1, 1); 
	}
	
	gsl_matrix * Z = build_Zero(-1, 6, NULL, -1);
	for (int k = 0; k < 7; k++){
		gsl_matrix_set(Z, iIndices[k], jIndices[k], values[k]);
	}

	make_RealMatrix_ColumnRateMatrix(Z, 6);

	return Z;

}

//_____________________________________________________________________________/


//________________Eigen Compiler for matrices with coords from files____________

int printEigen_RealMatrix_FromFile(gsl_matrix*(*Sample)(int, char *, gsl_rng *), char * fileName, int matrixSize, int nTrials, bool wantRealEigenValues){

	gsl_rng * r = setupRNG(1);
	gsl_matrix * M;
	gsl_vector_complex * eval;
	

	for (int t = 0; t < nTrials; t++){
		M = (*Sample)(matrixSize, fileName, r);
		eval = realNonSymmEigen(M, matrixSize);
		
		print_ComplexVector_EigenvalueEval(eval, matrixSize, wantRealEigenValues);

		gsl_matrix_free(M);
		gsl_vector_complex_free(eval);
	}
	gsl_rng_free(r);
	return 0;
}


//_____________________________________Eigen Compilation Functions*/

int print_Eigen_RealNonSymm(double mean, int n, double stdDev, int T, bool wantReal, gsl_matrix * (*f)(double, int, gsl_rng *, double), double scaleFactor) {
	gsl_vector_complex * eval;
	gsl_matrix * Sample;
	gsl_rng * r = setupRNG(1);
	

	for (int i = 0; i < T; i++) {
		Sample = (*f)(mean, n, r, stdDev);
		eval = realNonSymmEigen(Sample, n);
		if (wantReal) {
			for (int j = 0; j < n; j++) {
				printf("%f\n", (GSL_REAL(gsl_vector_complex_get(eval, j))) *(scaleFactor));
			}
		}
		else {
			for (int j = 0; j < n; j++) {
				printf("%f\n", (GSL_IMAG(gsl_vector_complex_get(eval, j))) *(scaleFactor));
			}
		}
		
		gsl_vector_complex_free(eval);
		gsl_matrix_free(Sample);
	}
	gsl_rng_free(r);
	return 1;
}
int save_Eigen_RealNonSymm(double mean, int n, double stdDev, int T, gsl_matrix * (*f)(double, int, gsl_rng *, double), double scaleFactor, char * reals_File, char * imags_File) {
	gsl_vector_complex * eval;
	gsl_matrix * Sample;
	gsl_rng * r = setupRNG(1);

	FILE * real_file;
	FILE * imag_file;

	real_file = fopen(reals_File, "w+");
	imag_file = fopen(imags_File, "w+");

	for (int i = 0; i < T; i++){
		Sample = (*f)(mean, n, r, stdDev);
		eval = realNonSymmEigen(Sample, n);
		for (int j = 0; j < n; j++)
		{
			fprintf(real_file, "%f\n", (GSL_REAL(gsl_vector_complex_get(eval, j))) * (scaleFactor));
		}
		for (int j = 0; j < n; j++)
		{
			fprintf(imag_file, "%f\n", (GSL_IMAG(gsl_vector_complex_get(eval, j))) *(scaleFactor));
		}
		
		gsl_vector_complex_free(eval);
		gsl_matrix_free(Sample);
	}
	gsl_rng_free(r);
	return 1;
}
int print_Eigen_RealNonSymm_MinusLowestMagnitudes(double mean, int n, double stdDev, int T, bool wantReal, gsl_matrix * (*f)(double, int, gsl_rng *, double), double scaleFactor) {
	gsl_vector_complex * eval;
	gsl_matrix * Sample;
	gsl_rng * r = setupRNG(1);
	

	for (int i = 0; i < T; i++) {
		Sample = (*f)(mean, n, r, stdDev);
		eval = realNonSymmEigen(Sample, n);
		if (wantReal) {
			double eigen[n];

			for (int j = 0; j < n; j++) {
				eigen[j] = GSL_REAL(gsl_vector_complex_get(eval, j)) * scaleFactor;

			}

			qsort(eigen, n, sizeof(double), comparedoublesByMagnitude);
			//printf("loop");
			//printf("%f\n", eigen[0]);
			for (int j = 1; j < n; j++) {

				printf("%f\n", eigen[j]);
			}
		}
		else {
			double eigen[n];

			for (int j = 0; j < n; j++) {
				eigen[j] = GSL_IMAG(gsl_vector_complex_get(eval, j)) * scaleFactor;

			}
			qsort(eigen, n, sizeof(double), comparedoublesByMagnitude);
			for (int j = 1; j < n; j++) {

				printf("%f\n", eigen[j]);
			}
		}
		
		gsl_vector_complex_free(eval);
		gsl_matrix_free(Sample);
	}
	gsl_rng_free(r);
	return 1;
}
int extractRealSymmEigen(double * arrayPointer, gsl_matrix * M, int matrixSize, double scaleFactor) {
	gsl_vector * eval = realSymmEigen(M, matrixSize);
	for (int j = 0; j < matrixSize; j++) {
		arrayPointer[j] = gsl_vector_get(eval, j)*(scaleFactor);
	}
	gsl_vector_free(eval);
	return 0;

}



int print_Eigen_RealSymm(double mean, int matrixSize, double stdDev, int Trials, gsl_matrix * (*build)(double, int, gsl_rng *, double), double scaleFactor) {
	//gsl_vector * eval;
	gsl_matrix * Sample;

	gsl_rng * r = setupRNG(1);

	double eigenArray[matrixSize];
	for (int i = 0; i < Trials; i++) {
		Sample = (*build)(mean, matrixSize, r, stdDev);
		extractRealSymmEigen(eigenArray, Sample, matrixSize, scaleFactor);
		print_DoubleArray(eigenArray, matrixSize);
		gsl_matrix_free(Sample);		
	}
	gsl_rng_free(r);
	return 0;
}
int write_doubleArray(double * eigenArray, int arraySize, FILE * fp){
	for (int i = 0; i < arraySize; i++)
	{
		fprintf(fp, "%f\n", eigenArray[i]);
	}
	return 0;
	
}
int save_Eigen_RealSymm(double mean, int matrixSize, double stdDev, int Trials, gsl_matrix * (*build)(double, int, gsl_rng *, double), double scaleFactor, char * fileName){
	gsl_matrix * Sample;
	gsl_rng * r = setupRNG(1);

	double eigenArray[matrixSize];
	FILE * fp = fopen(fileName, "w+");
	for (int i = 0; i < Trials; i++)
	{
		Sample = (*build)(mean, matrixSize, r, stdDev);
		extractRealSymmEigen(eigenArray, Sample, matrixSize, scaleFactor);
		write_doubleArray(eigenArray, matrixSize, fp);
		gsl_matrix_free(Sample);
	}
	
	fclose(fp);
	gsl_rng_free(r);
	return 0;
}
int save_Eigen_RealSymm_FromCoordFile(matrixInfo mI, eigenInfo eI, char * coordFile, double (*valueSampler)(gsl_rng *, double, double), char * saveFile, void (*modifiers[])(gsl_matrix *, int matrixSize), int modifiersSize){
	
	// Assembles coordinate arrays struct storing invariant matrix data to be passed to functions
	coordArrays cA;
	// Could hypothetically need all size*size spots in matrix so need that many slots in coordinate arrays
	cA.iIndices = malloc(sizeof(int)*mI.size*mI.size);
	cA.jIndices = malloc(sizeof(int)*mI.size*mI.size);	
	// fills the coord arrays from coord file and saves their length 
	cA.arraysSize = convert_CoordFile_ToCoordArrays(mI.size, cA.iIndices, cA.jIndices, coordFile);

	// samples a matrix, calculates eigen, and stores in a file
	gsl_matrix * Sample;
	gsl_rng * r = setupRNG(1);

	double eigenArray[mI.size];
	FILE * fp = fopen(saveFile, "w+");
	for (int i = 0; i < eI.numberTrials; i++)
	{
		// pass invariant matrix data through mI and cA plus a rng and valueSampler to use it
		Sample = generate_FromCoordArrays(mI, r, cA, valueSampler);
		for (int i = 0; i < modifiersSize; i++)
		{
			(*modifiers[i])(Sample, mI.size);
		}
		
		extractRealSymmEigen(eigenArray, Sample, mI.size, eI.scaleFactor);
		write_doubleArray(eigenArray, mI.size, fp);
		gsl_matrix_free(Sample);
	}
	
	fclose(fp);
	gsl_rng_free(r);
	return 0;
}


int print_Eigen_RealSymm_MinusLowestMagnitudes(double mean, int n, double stdDev, int T, gsl_matrix * (*f)(double, int, gsl_rng *, double), double scaleFactor) {
	gsl_vector * eval;
	gsl_matrix * Sample;
	//double rt2 = sqrt(2);
	gsl_rng * r = setupRNG(1);


	for (int i = 0; i < T; i++) {
		Sample = (*f)(mean, n, r, stdDev);
		eval = realSymmEigen(Sample, n);
		double eigen[n];

		for (int j = 0; j < n; j++) {
			eigen[j] = gsl_vector_get(eval, j) * scaleFactor;

		}
		qsort(eigen, n, sizeof(double), comparedoublesByMagnitude);
		for (int j = 1; j < n; j++) {

				printf("%f\n", eigen[j]);
		}
			gsl_vector_free(eval);
			gsl_matrix_free(Sample);
		
	}
	gsl_rng_free(r);
	return 1;
}
int print_Spacings_RealSymm_MinusLowestMagnitudes (double mean, int n, double stdDev, int T, gsl_matrix * (*f)(double, int, gsl_rng *, double), double scaleFactor) {


	gsl_vector * eval;
	gsl_matrix * Sample;
	gsl_rng * r = setupRNG(1);
	double sortedEval[n];
	for(int i = 0; i < T; i++){
		Sample = (*f)(mean, n, r, 1);
		eval = realSymmEigen(Sample, n);
		
		for (int j = 0; j < n; j++) {
			sortedEval[j] = (gsl_vector_get(eval, j)) * scaleFactor;
		}
		qsort(sortedEval, n, sizeof(double), comparedoublesByMagnitude);
		
		for (int j = 0; j < n - 1; j++){
			sortedEval[j] = sortedEval[j+1];
		}
		//printf("%f\n", sortedEval[0]);
		qsort(sortedEval, n - 1, sizeof(double), comparedoubles);

		for (int j = 0; j < n - 2; j++) {
			printf("%f\n", ((sortedEval[j + 1]) - (sortedEval[j])));
		}
		gsl_vector_free(eval);
		gsl_matrix_free(Sample);
			
	}
	gsl_rng_free(r);
	return 1;
}

int print_Eigen_RealImag(double mean, int matrixSize, double stdDev, int T, gsl_matrix * (*f)(double, int, gsl_rng *, double), double n_a){


	gsl_rng * r = setupRNG(1);

	for(int i = 0; i < T; i++)
	{
		gsl_matrix * M = (*f)(mean, matrixSize, r, stdDev);
		gsl_vector_complex * eval = realNonSymmEigen(M, matrixSize);

		for(int i = 0; i < matrixSize; i++)
		{
			float realVal = GSL_REAL(gsl_vector_complex_get(eval, i));
			float imagVal = GSL_IMAG(gsl_vector_complex_get(eval, i));
			printf("%f %f\n", realVal, imagVal);		
		}
		gsl_matrix_free(M);
		gsl_vector_complex_free(eval);
	}
	gsl_rng_free(r);

	



	return 0;
}

//______________________________________________________________________________

int plot_Eigen(matrixInfo mI, eigenInfo eI, char * pngName, int (*coordGenerator)(int, char *)){

	typedef void (*matrixModifier)(gsl_matrix *, int matrixSize);
	
	matrixModifier modifiers[1] = {modify_RealMatrix_ColumnRateMatrix};

	
	char norm[1000];
	char plot[1000];
	(*coordGenerator)(mI.size, "temp.cycle");
	save_Eigen_RealSymm_FromCoordFile(mI, eI, "temp.cycle", sample_Exponential, "temp.eigen", modifiers, 1);
	system("./histogram -10 10 300 temp.eigen > temp.hist");
	sprintf(norm, "./normalize %d %d temp.hist > temp.norm", mI.size, eI.numberTrials);
	system(norm);

	sprintf(plot, "gnuplot -e \" set term png size 400, 300; set output '%s'; plot 'temp.norm' u 1:3 \" ", pngName);
	system(plot);
	system("rm *.cycle; rm *.hist; rm *.eigen; rm *.norm");

	return 0;
}


//_____________________________________________________________________________/
int main(int argc, char * argv[]){


	char name[100];
	if (argc > 1){
		matrixInfo mI = {atoi(argv[1]), 1, 1};
		eigenInfo eI = {1/sqrt(atoi(argv[1])), 1000000};
		plot_Eigen(mI, eI, "test.png", singleCycle_WriteToFile);
	}
	else{

		for (int i = 1; i < 101; i++)
		{
			int s = i;
			matrixInfo mI = {s, 1, 1};
			eigenInfo eI = {1/sqrt(s), 1000000};
			
			sprintf(name, "1000000T_%d-doublyCycle.png", s);
			printf("Plotting %s\n", name);

			clock_t start, end;
			double cpu_time_used;
			start = clock();

			plot_Eigen(mI, eI, name, doubleCycle_WriteToFile);

			end = clock();
			cpu_time_used =((double) (end - start)) /CLOCKS_PER_SEC;
			printf("This took %f minutes\n", cpu_time_used / 60);
		}
		system("mkdir DoublyCycles");
		system("mv *.png DoublyCycles");
		for (int i = 1; i < 101; i++)
		{
			int s = i * 101;
			matrixInfo mI = {s, 1, 1};
			eigenInfo eI = {1/sqrt(s), 10000};
			
			sprintf(name, "10000T_%d-doublyCycle.png", s);
			printf("Plotting %s\n", name);

			clock_t start, end;
			double cpu_time_used;
			start = clock();

			plot_Eigen(mI, eI, name, doubleCycle_WriteToFile);

			end = clock();
			cpu_time_used =((double) (end - start)) /CLOCKS_PER_SEC;
			printf("This took %f minutes\n", cpu_time_used / 60);
		}
		system("mv *.png DoublyCycles");

		for (int i = 1; i < 101; i++)
		{
			int s = i*101;
			matrixInfo mI = {s, 1, 1};
			eigenInfo eI = {1/sqrt(s), 10000};
			
			sprintf(name, "10000T_%d-singlyCycle.png", s);
			printf("Plotting %s\n", name);

			clock_t start, end;
			double cpu_time_used;
			start = clock();

			plot_Eigen(mI, eI, name, singleCycle_WriteToFile);

			end = clock();
			cpu_time_used =((double) (end - start)) /CLOCKS_PER_SEC;
			printf("This took %f minutes\n", cpu_time_used / 60);
		}

		system("mkdir SinglyCycles");
		system("mv *.png SinglyCycles");
		
		for (int i = 1; i < 101; i++)
		{
			int s = i*101;
			matrixInfo mI = {s, 1, 1};
			eigenInfo eI = {1/sqrt(s), 1000000};
			
			sprintf(name, "1000000T_%d-singlyCycle.png", s);
			printf("Plotting %s\n", name);

			clock_t start, end;
			double cpu_time_used;
			start = clock();

			plot_Eigen(mI, eI, name, singleCycle_WriteToFile);

			end = clock();
			cpu_time_used =((double) (end - start)) /CLOCKS_PER_SEC;
			printf("This took %f minutes\n", cpu_time_used / 60);
		}	

		system("mv *.png SinglyCycles");


		for (int i = 1; i < 101; i++)
		{
			int s = i * 101;
			matrixInfo mI = {s, 1, 1};
			eigenInfo eI = {1/sqrt(s), 1000000};
			
			sprintf(name, "1000000T_%d-doublyCycle.png", s);
			printf("Plotting %s\n", name);

			clock_t start, end;
			double cpu_time_used;
			start = clock();

			plot_Eigen(mI, eI, name, doubleCycle_WriteToFile);

			end = clock();
			cpu_time_used =((double) (end - start)) /CLOCKS_PER_SEC;
			printf("This took %f minutes\n", cpu_time_used / 60);
		}
		system("mv *.png DoublyCycles");
	}
	
	printf("Somehow Done! Files stored in relevant Folders\n");
	return 0;
}

/*
    int * iIndices = malloc(sizeof(int)*25);
	int * jIndices = malloc(sizeof(int)*25);
	double * values = malloc(sizeof(double)*25);
	int tripleArraysSize;

	gsl_rng * r = setupRNG(1);
	singleCycle_WriteToFile(5, "test.txt");
	gsl_matrix * M = import_RealMatrix_FromCoordFile_Exponential(5, "test.txt", r);
	
	print_RealMatrix(M, 5);
	
	tripleArraysSize = convert_CoordFile_ToCoordArrays(5, iIndices, jIndices, "test.txt");
	import_RealMatrix_FromCoordArrays_Exponential(5, tripleArraysSize, iIndices, jIndices, r);
	convert_RealMatrix_ToTripleArrays(5, iIndices, jIndices, values, M);
	printf("\n");
    print_IntArray(iIndices, tripleArraysSize);
	printf("\n");
	print_IntArray(jIndices, tripleArraysSize);
	printf("\n");
	print_DoubleArray(values, tripleArraysSize);

    free(iIndices);
	free(jIndices);
	free(values);
	gsl_rng_free(r);
	gsl_matrix_free(M);*/

/*
	double mean;
	double stdDev;
	int MatrixSize;
	int Trials;

	double user = 0;
	if (argc > 1){
		if (argc < 3){
		printf("%s", argv[1]);
		if (strcmp(argv[1], "ESRE")==0){
			mean = 1;
			stdDev = 1;
			MatrixSize = 10;
			Trials = 100000;
		}
		if (strcmp(argv[1], "FromFile")==0){
			char * fileName = argv[2];
			int matrixSize = atoi(argv[3]);
			int nTrials = atoi(argv[4]);
			bool wantRealValues = strcmp(argv[5], "real");

			printEigen_RealMatrix_FromFile(assembleRealMatrix_FromFile_EGRE, fileName, matrixSize, nTrials, wantRealValues);
		}
		else{
		mean = 0;
		stdDev = 1;
		MatrixSize = 10;
		Trials = 10;
		}
	}
	else{
		mean = atof(argv[1]);
		stdDev = atof(argv[2]);
		MatrixSize = atoi(argv[3]);
		Trials = atoi(argv[4]);
	}
	}
	
	
	
	
	
	

	user = mean + stdDev + MatrixSize + Trials;
		//printf("finishing task took %f seconds\n", cpu_time_used);
	*/
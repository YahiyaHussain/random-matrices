//2n x 2n matrix keep in mind
gsl_matrix_complex * SampleGSE(int n, double stdDev, int nTrial, gsl_rng * r){
	gsl_matrix_complex * GSE = gsl_matrix_complex_alloc(2*n,2*n);

	//gsl_rng * r = setupRNG(nTrial);


	gsl_matrix_complex_view A = gsl_matrix_complex_submatrix(GSE, 0, 0, n, n);
	gsl_matrix_complex_view B = gsl_matrix_complex_submatrix(GSE, 0, n, n, n);
	for (int i = 0; i < n; i++){
		for (int j = 0; j < n; j++){
 			double a1 = gsl_ran_gaussian(r, stdDev);
			double a2 = gsl_ran_gaussian(r, stdDev);
			double b1 = gsl_ran_gaussian(r, stdDev);
			double b2 = gsl_ran_gaussian(r, stdDev);

			gsl_matrix_complex_set(&A.matrix, i, j, gsl_complex_rect(a1, a2));
			gsl_matrix_complex_set(&B.matrix, i, j, gsl_complex_rect(b1, b2));
		}
	}
	gsl_matrix_complex_view C = gsl_matrix_complex_submatrix(GSE, n, 0, n, n);
	gsl_matrix_complex_view D = gsl_matrix_complex_submatrix(GSE, n, n, n, n);
	for (int i = 0; i < n; i++){
		for (int j = 0; j < n; j++){
			gsl_complex c = gsl_complex_conjugate(gsl_matrix_complex_get(&B.matrix, i, j));
			c = gsl_complex_rect(-1 * GSL_REAL(c), -1 *GSL_IMAG(c));
			gsl_complex d = gsl_complex_conjugate(gsl_matrix_complex_get(&A.matrix, i, j));
			gsl_matrix_complex_set(&C.matrix, i, j, c);
			gsl_matrix_complex_set(&D.matrix, i, j, d);
		}
	}
	//gsl_rng_free(r);

	symmetrizeComplexMatrix(GSE, 2*n);
	return GSE;
}


gsl_matrix * SampleMatrix1a(int n, double stdDev, gsl_rng * r){
	gsl_matrix * M = gsl_matrix_alloc(n*2, n*2);
	gsl_matrix_view A = gsl_matrix_submatrix(M, 0, 0, n, n);
	gsl_matrix_view B = gsl_matrix_submatrix(M, 0, n, n, n);
	gsl_matrix_view C = gsl_matrix_submatrix(M, n, 0, n, n);
	gsl_matrix_view D = gsl_matrix_submatrix(M, n, n, n, n);

	gsl_matrix_set_zero(&A.matrix);
	gsl_matrix_set_zero(&D.matrix);

	for (int i = 0; i < n; i++){
		gsl_matrix_set(&B.matrix, i, i, gsl_ran_gaussian(r, stdDev));
	}


	for (int i = 0; i < n; i++){
		for (int j = 0; j < n; j++){
			gsl_matrix_set(&C.matrix, i, j, gsl_ran_gaussian(r, stdDev));
		}
	}
	symmetrizeRealMatrix(&C.matrix, n);
	return M;
}


gsl_matrix * SampleMatrix1b(int n, double stdDev, int nTrial, gsl_rng * r){
	gsl_matrix * M = gsl_matrix_alloc(n, n);
	for (int i = 0; i < n; i++){
		for (int j = 0; j < n; j++){
			gsl_matrix_set(M, i, j, gsl_ran_gaussian_pdf(gsl_ran_gaussian(r, stdDev), stdDev));
		}
	}
	//printRealMatrix(M, n);
	gsl_vector_view v;
	for (int i = 0; i < n; i++){
		v = gsl_matrix_row(M, i);
		gsl_vector_scale(&v.vector, 1 / gsl_blas_dasum(&v.vector));
	}
	//printRealMatrix(M, n);
	return M;
}


gsl_matrix * SampleEGRE(double mean, int n, gsl_rng * r, double n_a) {
	gsl_matrix * M = gsl_matrix_alloc(n, n);
	double value;
	double sum;
	for (int j = 0; j < n; j++) {
		sum = 0;
		for (int i = 0; i < n; i++) {
			if (i != j) {
				value = gsl_rng_uniform(r);
				value = -1*(value - 1);
				value = log(value);
				value = -1 * mean * value;
				gsl_matrix_set(M, i, j, value);
				sum += value;
			}
		}
		gsl_matrix_set(M, j, j, -1 * sum);
	}
	//printRealMatrix(M, n);
	return M;
}
gsl_matrix * SampleEGRE_ATILDA(double mean, int n, gsl_rng * r, double n_a) {
	gsl_matrix * A = SampleEGRE(mean, n, r, n_a);

	for (int i = 0; i < n; i++) {
		for (int j = i; j < n; j++) {
			if (i != j) {
				double value = gsl_matrix_get(A, i, j);
				gsl_matrix_set(A, i, j, value - mean);
				gsl_matrix_set(A, j, i, value - mean);
			}
			else {
				gsl_matrix_set(A, i, j, gsl_matrix_get(A, i, j) + ((n - 1)*(mean)));
			}
		}
	}
	return A;
}

gsl_matrix * SampleESRE_ZeroDiagonal(double mean, int n, gsl_rng * r, double n_a) {
	gsl_matrix * ESRE = SampleESRE(mean, n, r, n_a);
	for (int i = 0; i < n; i++) {
		gsl_matrix_set(ESRE, i, i, 0);
	}
	return ESRE;
}
gsl_matrix * SampleESRE_ATILDA_ZeroDiagonal(double mean, int n, gsl_rng * r, double n_a) {
	gsl_matrix * A = SampleESRE_ATILDA(mean, n, r, n_a);

	for (int i = 0; i < n; i++) {
		gsl_matrix_set(A, i, i, 0);
	}
	return A;
}

gsl_matrix * SampleGOE_Uniform1Var(double mean, int n, gsl_rng * r, double n_a){
	gsl_matrix * M = gsl_matrix_alloc(n, n);
	for(int i = 0; i < n; i++)
		for(int j = i; j < n; j++){
			gsl_matrix_set(M, i, j, gsl_ran_gaussian(r, 1));
			if (j != i){
				gsl_matrix_set(M, j, i, gsl_matrix_get(M, i, j));
			}
		}


	//symmetrizeRealMatrix(M, n);
	return M;
}
gsl_matrix * SampleGOE_DifferentVar(double mean, int n, gsl_rng * r, double n_a){
	gsl_matrix * M = gsl_matrix_alloc(n,n);
	for (int i = 0; i < n; i++){
		for (int j = i; j < n; j++){
			if(i == j){
				gsl_matrix_set(M, i, j, gsl_ran_gaussian(r, 1));
			}
			else{
				gsl_matrix_set(M, i, j, gsl_ran_gaussian(r, sqrtf(.5)));
				gsl_matrix_set(M, j, i, gsl_matrix_get(M, i, j));
			}
		}
	}

	return M;
}

gsl_matrix * SampleGOE2_Uniform1Var(double mean, int n, gsl_rng * r, double n_a){
	gsl_matrix * M = gsl_matrix_alloc(n,n);
	for(int i = 0; i < n; i++)
		for(int j = 0; j < n; j++)
			gsl_matrix_set(M, i, j, gsl_ran_gaussian(r, 1));
	
	symmetrizeRealMatrix(M, n);
	return M;
}

gsl_matrix * SampleGOE2_DifferentVar(double mean, int n, gsl_rng * r, double n_a){
	gsl_matrix * M = gsl_matrix_alloc(n,n);
	for(int i = 0; i < n; i++)
		for(int j = 0; j < n; j++)
		{
			if (i == j)
				gsl_matrix_set(M, i, i, gsl_ran_gaussian(r, 1));
			else
				gsl_matrix_set(M, i, j, gsl_ran_gaussian(r, 1));
		}
	symmetrizeRealMatrix(M, n);
	return M;
}

gsl_matrix_complex* sampleGUE(int n, double stdDev, int nTrial, gsl_rng * r){
	gsl_matrix_complex * GUE = gsl_matrix_complex_alloc(n,n);

	//gsl_rng * r = setupRNG(nTrial);

	for (int i = 0; i < n; i++){
		for (int j = 0; j < n; j++){
			double s1 = gsl_ran_gaussian(r, stdDev);
			double s2 = gsl_ran_gaussian(r, stdDev);
			gsl_matrix_complex_set(GUE, i, j, gsl_complex_rect(s1, s2));
		}
	}
	//gsl_rng_free(r);

	symmetrizeComplexMatrix(GUE, n);
	return GUE;
}

gsl_matrix * SampleESRE_SymmatrizeWithTranspose(double mean, int n, gsl_rng * r, double n_a) {

if (n > 1) {
		gsl_matrix * M = gsl_matrix_alloc(n, n);
		double value;
		for (int j = 0; j < n; j++) {
			for (int i = 0; i < n; i++) {
					value = valueSample_ESRE(r, mean)
					gsl_matrix_set(M, i, j, value);
			}

		}

		symmetrizeRealMatrix(M, n);

		double sum;
		for (int j = 0; j < n; j++){
			sum = 0;
			for (int i = 0; i < n; i++){
				if (i != j) {
					sum += gsl_matrix_get(M, i, j);
				}
			gsl_matrix_set(M, j, j, -1 * sum);
			}
		}

		return M;
	}
	else {
		return NULL;
	}
}

























gsl_matrix * Sample_DoublyCycleRateMatrix_Exponential(double mean, int n, gsl_rng * r, double stdDev){

	gsl_matrix * S = Sample_SinglyCycleRateMatrix_Exponential(mean,n,r,stdDev);

	symmetrizeRealMatrix(S, n);

	return S;
}



















#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
int main(int argc, char const *argv[]) {
  int matrixSize = atoi(argv[1]);
  int Trials = atoi(argv[2]);
  char * line = NULL;
  size_t len = 0;
  ssize_t read;
  FILE * fp = fopen(argv[3], "r");

  int oneCount = 0;
  int rangeCount = 0;
  if (fp == NULL){
    printf("error couldnt open file");
    return 1;
  }
  while ((read = getline(&line, &len, fp)) != -1){
    if (line[0] == 'f'){ break; }
    if (fabs(atof(line)) > 0 && fabs(atof(line)) < 1){ rangeCount++; }
    else if (strcmp(line, "1.000000")){ oneCount++; }
    else{ printf("%s\n", line); }
  }
  printf("Number of ones: %d; Number of floats [0,1]: %d\n", oneCount,rangeCount);
  if (oneCount + rangeCount == Trials * matrixSize){
    printf("Thus this is a Transition matrix.\n");
  }
  else{
    printf("Thus this is not a Transition matrix.\n");
  }
  return 0;
}







/*
	int * iIndices = malloc(sizeof(int)*25);
	int * jIndices = malloc(sizeof(int)*25);
	double * values = malloc(sizeof(double)*25);
	int tripleArraysSize;

	gsl_rng * r = setupRNG(1);
	singleCycle_WriteToFile(5, "test.txt");
	gsl_matrix * M = import_RealMatrix_FromCoordFile_Exponential(5, "test.txt", r);

	print_RealMatrix(M, 5);

	tripleArraysSize = convert_CoordFile_ToCoordArrays(5, iIndices, jIndices, "test.txt");
	import_RealMatrix_FromCoordArrays_Exponential(5, tripleArraysSize, iIndices, jIndices, r);
	convert_RealMatrix_ToTripleArrays(5, iIndices, jIndices, values, M);
	printf("\n");
	print_IntArray(iIndices, tripleArraysSize);
	printf("\n");
	print_IntArray(jIndices, tripleArraysSize);
	printf("\n");
	print_DoubleArray(values, tripleArraysSize);

	free(iIndices);
	free(jIndices);
	free(values);
	gsl_rng_free(r);
	gsl_matrix_free(M);*/

	/*
		double mean;
		double stdDev;
		int MatrixSize;
		int Trials;

		double user = 0;
		if (argc > 1){
			if (argc < 3){
			printf("%s", argv[1]);
			if (strcmp(argv[1], "ESRE")==0){
				mean = 1;
				stdDev = 1;
				MatrixSize = 10;
				Trials = 100000;
			}
			if (strcmp(argv[1], "FromFile")==0){
				char * fileName = argv[2];
				int matrixSize = atoi(argv[3]);
				int nTrials = atoi(argv[4]);
				bool wantRealValues = strcmp(argv[5], "real");

				printEigen_RealMatrix_FromFile(assembleRealMatrix_FromFile_EGRE, fileName, matrixSize, nTrials, wantRealValues);
			}
			else{
			mean = 0;
			stdDev = 1;
			MatrixSize = 10;
			Trials = 10;
			}
		}
		else{
			mean = atof(argv[1]);
			stdDev = atof(argv[2]);
			MatrixSize = atoi(argv[3]);
			Trials = atoi(argv[4]);
		}
		}







		user = mean + stdDev + MatrixSize + Trials;
			//printf("finishing task took %f seconds\n", cpu_time_used);
		*/












	int print_Eigen_RealSymm_MinusLowestMagnitudes(double mean, int n, double stdDev, int T, gsl_matrix * (*f)(double, int, gsl_rng *, double), double scaleFactor) {
	gsl_vector * eval;
	gsl_matrix * Sample;
	//double rt2 = sqrt(2);
	gsl_rng * r = setupRNG(1);


	for (int i = 0; i < T; i++) {
		Sample = (*f)(mean, n, r, stdDev);
		eval = realSymmEigen(Sample, n);
		double eigen[n];

		for (int j = 0; j < n; j++) {
			eigen[j] = gsl_vector_get(eval, j) * scaleFactor;

		}
		qsort(eigen, n, sizeof(double), comparedoublesByMagnitude);
		for (int j = 1; j < n; j++) {

				printf("%f\n", eigen[j]);
		}
			gsl_vector_free(eval);
			gsl_matrix_free(Sample);
		
	}
	gsl_rng_free(r);
	return 1;
}
int print_Spacings_RealSymm_MinusLowestMagnitudes (double mean, int n, double stdDev, int T, gsl_matrix * (*f)(double, int, gsl_rng *, double), double scaleFactor) {


	gsl_vector * eval;
	gsl_matrix * Sample;
	gsl_rng * r = setupRNG(1);
	double sortedEval[n];
	for(int i = 0; i < T; i++){
		Sample = (*f)(mean, n, r, 1);
		eval = realSymmEigen(Sample, n);
		
		for (int j = 0; j < n; j++) {
			sortedEval[j] = (gsl_vector_get(eval, j)) * scaleFactor;
		}
		qsort(sortedEval, n, sizeof(double), comparedoublesByMagnitude);
		
		for (int j = 0; j < n - 1; j++){
			sortedEval[j] = sortedEval[j+1];
		}
		//printf("%f\n", sortedEval[0]);
		qsort(sortedEval, n - 1, sizeof(double), comparedoubles);

		for (int j = 0; j < n - 2; j++) {
			printf("%f\n", ((sortedEval[j + 1]) - (sortedEval[j])));
		}
		gsl_vector_free(eval);
		gsl_matrix_free(Sample);
			
	}
	gsl_rng_free(r);
	return 1;
}

int print_Eigen_RealImag(double mean, int matrixSize, double stdDev, int T, gsl_matrix * (*f)(double, int, gsl_rng *, double), double n_a){


	gsl_rng * r = setupRNG(1);

	for(int i = 0; i < T; i++)
	{
		gsl_matrix * M = (*f)(mean, matrixSize, r, stdDev);
		gsl_vector_complex * eval = realNonSymmEigen(M, matrixSize);

		for(int i = 0; i < matrixSize; i++)
		{
			float realVal = GSL_REAL(gsl_vector_complex_get(eval, i));
			float imagVal = GSL_IMAG(gsl_vector_complex_get(eval, i));
			printf("%f %f\n", realVal, imagVal);		
		}
		gsl_matrix_free(M);
		gsl_vector_complex_free(eval);
	}
	gsl_rng_free(r);

	



	return 0;
}












int print_Eigen_RealNonSymm_MinusLowestMagnitudes(double mean, int n, double stdDev, int T, bool wantReal, gsl_matrix * (*f)(double, int, gsl_rng *, double), double scaleFactor) {
	gsl_vector_complex * eval;
	gsl_matrix * Sample;
	gsl_rng * r = setupRNG(1);
	

	for (int i = 0; i < T; i++) {
		Sample = (*f)(mean, n, r, stdDev);
		eval = realNonSymmEigen(Sample, n);
		if (wantReal) {
			double eigen[n];

			for (int j = 0; j < n; j++) {
				eigen[j] = GSL_REAL(gsl_vector_complex_get(eval, j)) * scaleFactor;

			}

			qsort(eigen, n, sizeof(double), comparedoublesByMagnitude);
			//printf("loop");
			//printf("%f\n", eigen[0]);
			for (int j = 1; j < n; j++) {

				printf("%f\n", eigen[j]);
			}
		}
		else {
			double eigen[n];

			for (int j = 0; j < n; j++) {
				eigen[j] = GSL_IMAG(gsl_vector_complex_get(eval, j)) * scaleFactor;

			}
			qsort(eigen, n, sizeof(double), comparedoublesByMagnitude);
			for (int j = 1; j < n; j++) {

				printf("%f\n", eigen[j]);
			}
		}
		
		gsl_vector_complex_free(eval);
		gsl_matrix_free(Sample);
	}
	gsl_rng_free(r);
	return 1;
}








int save_Eigen_RealNonSymm(double mean, int n, double stdDev, int T, gsl_matrix * (*f)(double, int, gsl_rng *, double), double scaleFactor, char * reals_File, char * imags_File) {
	gsl_vector_complex * eval;
	gsl_matrix * Sample;
	gsl_rng * r = setupRNG(1);

	FILE * real_file;
	FILE * imag_file;

	real_file = fopen(reals_File, "w+");
	imag_file = fopen(imags_File, "w+");

	for (int i = 0; i < T; i++){
		Sample = (*f)(mean, n, r, stdDev);
		eval = realNonSymmEigen(Sample, n);
		for (int j = 0; j < n; j++)
		{
			fprintf(real_file, "%f\n", (GSL_REAL(gsl_vector_complex_get(eval, j))) * (scaleFactor));
		}
		for (int j = 0; j < n; j++)
		{
			fprintf(imag_file, "%f\n", (GSL_IMAG(gsl_vector_complex_get(eval, j))) *(scaleFactor));
		}
		
		gsl_vector_complex_free(eval);
		gsl_matrix_free(Sample);
	}
	gsl_rng_free(r);
	return 1;
}











int print_Eigen_RealNonSymm(double mean, int n, double stdDev, int T, bool wantReal, gsl_matrix * (*f)(double, int, gsl_rng *, double), double scaleFactor) {
	gsl_vector_complex * eval;
	gsl_matrix * Sample;
	gsl_rng * r = setupRNG(1);
	

	for (int i = 0; i < T; i++) {
		Sample = (*f)(mean, n, r, stdDev);
		eval = realNonSymmEigen(Sample, n);
		if (wantReal) {
			for (int j = 0; j < n; j++) {
				printf("%f\n", (GSL_REAL(gsl_vector_complex_get(eval, j))) *(scaleFactor));
			}
		}
		else {
			for (int j = 0; j < n; j++) {
				printf("%f\n", (GSL_IMAG(gsl_vector_complex_get(eval, j))) *(scaleFactor));
			}
		}
		
		gsl_vector_complex_free(eval);
		gsl_matrix_free(Sample);
	}
	gsl_rng_free(r);
	return 1;
}











//_____________________Test Example Functions___________________________________
gsl_matrix * test_ContinMasterEquation_ESRE(double n_a1, int n, gsl_rng * r, double n_a){
	//gsl_rng * r = setupRNG(1);
	
	int iIndices[7] = {0, 3, 3, 0, 2, 5, 1};
	int jIndices[7] = {1, 0, 4, 2, 3, 4, 5};

	double values[7];
	for(int k = 0; k < 7; k++){
		values[k] = sample_Exponential(r, 1, 1); 
	}
	
	gsl_matrix * Z = build_Zero(-1, 6, NULL, -1);
	for (int k = 0; k < 7; k++){
		gsl_matrix_set(Z, iIndices[k], jIndices[k], values[k]);
	}

	make_RealMatrix_ColumnRateMatrix(Z, 6);

	return Z;

}








// averages a matrix with its transpose
int comparedoubles(const void * a, const void * b){
	const double fa = * (const double *) a;
	const double fb = * (const double *) b;

	return (fa > fb) - (fb > fa);
}
int comparedoublesByMagnitude(const void *a, const void *b) {
	const double *fa = (const double *)a;
	const double *fb = (const double *)b;
	return ((fabs(*fa) > fabs(*fb)) - (fabs(*fa) < fabs(*fb)));
}



gsl_vector * hermEigen(gsl_matrix_complex * M, int n){
	gsl_matrix_complex_view view = gsl_matrix_complex_view_array(M->data,n,n);
	gsl_vector * eval = gsl_vector_alloc(n);
	gsl_eigen_herm_workspace * w = gsl_eigen_herm_alloc(n);
	gsl_eigen_herm(&view.matrix,eval,w);
	gsl_eigen_herm_free(w);
	return eval;
}




int printEigen_RealMatrix_FromFile(gsl_matrix*(*Sample)(int, char *, gsl_rng *), char * fileName, int matrixSize, int nTrials, bool wantRealEigenValues){

	gsl_rng * r = setupRNG(1);
	gsl_matrix * M;
	gsl_vector_complex * eval;
	

	for (int t = 0; t < nTrials; t++){
		M = (*Sample)(matrixSize, fileName, r);
		eval = realNonSymmEigen(M, matrixSize);
		
		print_ComplexVector_EigenvalueEval(eval, matrixSize, wantRealEigenValues);

		gsl_matrix_free(M);
		gsl_vector_complex_free(eval);
	}
	gsl_rng_free(r);
	return 0;
}


gsl_matrix * make_RealMatrix_ColumnRateMatrix(gsl_matrix * M, int matrixSize){
	double sum;
	for (int j = 0; j < matrixSize; j++){
		sum = 0;
		for (int i = 0; i < matrixSize; i++){
			if (i != j)
				sum += gsl_matrix_get(M, i, j);
		}
		gsl_matrix_set(M, j, j, -sum);
	}

	return M;
}





int print_Eigen_RealSymm(double mean, int matrixSize, double stdDev, int Trials, gsl_matrix * (*build)(double, int, gsl_rng *, double), double scaleFactor) {
	//gsl_vector * eval;
	gsl_matrix * Sample;

	gsl_rng * r = setupRNG(1);

	double eigenArray[matrixSize];
	for (int i = 0; i < Trials; i++) {
		Sample = (*build)(mean, matrixSize, r, stdDev);
		extract_RealSymmEigen(eigenArray, Sample, matrixSize, scaleFactor);
		print_DoubleArray(eigenArray, matrixSize);
		gsl_matrix_free(Sample);		
	}
	gsl_rng_free(r);
	return 0;
}




//______________________________________________________________________________

gsl_rng * setupRNG(int nTrial){
	gsl_rng * r;
	const gsl_rng_type * T;
	gsl_rng_env_setup();
	T = gsl_rng_default;
	r = gsl_rng_alloc(T);
	gsl_rng_set(r, time(0) * nTrial);
	return r;
}


	// char name[100];
	// matrixInfo mI = {5000, 1, 1};
	// eigenInfo eI = {1, 1};
	// histInfo hI = {-45, 1, 500};
		
	// singleCycle_WriteToFile(mI.size, "cycle.temp");
	// matrixModifier modifiers[1] = {modify_RealMatrix_ColumnRateMatrix};
	// start = clock();
	// save_Eigen_RealNonSymm_FromCoordFile_LogScale(mI, eI, "cycle.temp", sample_Exponential, "eigen.temp", modifiers, 1, false);
	// end = clock();

	// printf("This whole program took %f seconds\n", (double)(start - end)/CLOCKS_PER_SEC);

	// if (argc > 2){

	// 	matrixInfo mI = {atoi(argv[2]), 1, 1};
	// 	eigenInfo eI = {1/sqrt(atoi(argv[2])), ceil(2000000.0/atoi(argv[2]))};
	// 	histInfo hI = {-45, 1, 500};

	// 	char pngname[1000];
	// 	if (strcmp(argv[1], "Single") == 0){
	// 		sprintf(pngname, "%d-SinglyCycle.png", atoi(argv[2]));		
	// 		plot_Eigen_Complex(mI, eI, hI, pngname, singleCycle_WriteToFile, true);
	// 	}
	// 	if (strcmp(argv[1], "Double") == 0){
	// 		sprintf(pngname, "%d-DoublyCycle.png", atoi(argv[2]));		
	// 		plot_Eigen_Complex(mI, eI, hI, pngname, doubleCycle_WriteToFile, false);
	// 	}
	// 	if((strcmp(argv[1], "Double") != 0) && (strcmp(argv[1], "Single") != 0)){
	// 		printf("ERROR INVALID 1ST ARGUMENT\n");
	// 	}
	// }
	// else {
	// 	if (strcmp(argv[1], "Single") == 0){
	// 		for (int i = 1; i < 10; i++)
	// 		{
	// 		int s = i * 50;

	// 		matrixInfo mI = { s, 1, 1 };
	// 		eigenInfo eI = { 1 / sqrt(s), ceil(2000000.0 / i) };
	// 		histInfo hI = { -45, 1, 500 };

	// 		double cpu_time_used;
	// 		clock_t start, end;

	// 		sprintf(name, "%d-singlyCycle.png", s);
	// 		printf("Plotting %s\n", name);
	// 		start = clock();
	// 		plot_Eigen_Complex(mI, eI, hI, name, singleCycle_WriteToFile, true);
	// 		end = clock();
	// 		cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;
	// 		printf("This whole program took %f minutes\n", cpu_time_used / 60);
	// 		fflush(stdout);
	// 		}
	// 	system("mkdir SinglyCycles"); 	
	// 	system("mv *.png SinglyCycles");
	// 	}


		/*if (strcmp(argv[1], "Double") == 0){
			for (int i = 2; i < 100; i++)
		{
			int s = i;
			matrixInfo mI = { s, 1, 1 };

			eigenInfo eI = { 1 / sqrt(s), ceil(1000000.0 / i) };
			histInfo hI = { -10, 10, 300 };

			double cpu_time_used;
			clock_t start, end;

			sprintf(name, "%d-doublyCycle.png", s);
			printf("Plotting %s\n", name);
			start = clock();
			plot_Eigen_Complex(mI, eI, hI, name, doubleCycle_WriteToFile, false);
			end = clock();
			cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;
			printf("\n\n\n");
			printf("This whole program took %f minutes\n", cpu_time_used / 60);
			printf("\n\n\n");
			fflush(stdout);
		}
		system("mkdir DoublyCycles"); 	
		system("mv *.png DoublyCycles");
		}*/
		

	//}
	
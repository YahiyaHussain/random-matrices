#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_blas.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_statistics_double.h>

#include "structs.h"



//______________________________modify matrices_______________________________
void modify_RealMatrix_Symmetrize(gsl_matrix * M, int n){
	gsl_matrix * transpose = gsl_matrix_alloc(n,n);
	gsl_matrix_transpose_memcpy(transpose, M);
	gsl_matrix_add(M, transpose);
	gsl_matrix_scale(M, 0.5);
	gsl_matrix_free(transpose);
	return;
}
void modify_ComplexMatrix_Symmetrize(gsl_matrix_complex * M, int n){
	gsl_matrix_complex * transpose = gsl_matrix_complex_alloc(n, n);
	gsl_matrix_complex_transpose_memcpy(transpose, M);
	gsl_matrix_complex_add(M, transpose);
	gsl_matrix_complex_scale(M, gsl_complex_rect(0.5, 0));
	gsl_matrix_complex_free(transpose);
	return;
}
void modify_RealMatrix_ColumnRateMatrix(gsl_matrix * M, int matrixSize){
	double sum;
	for (int j = 0; j < matrixSize; j++){
		sum = 0;
		for (int i = 0; i < matrixSize; i++){
			if (i != j)
				sum += gsl_matrix_get(M, i, j);
		}
		gsl_matrix_set(M, j, j, -sum);
	}

	return;
}
void modify_RealMatrix_ColumnTransitionMatrix(gsl_matrix * M, int matrixSize){
	double sum;
	for (int j = 0; j < matrixSize; j++){
		sum = 0;
		for (int i = 0; i < matrixSize; i++){
				sum += gsl_matrix_get(M, i, j);
		}
		for (int i = 0; i < matrixSize; i++){
			gsl_matrix_set(M, i, j, gsl_matrix_get(M, i, j) / sum);
		}
	}

	return;
}



//_________________________________build matrices_________________________

gsl_matrix* build_Zero(double n_a1, int n, gsl_rng * n_a2, double n_a3){
	gsl_matrix * Z = gsl_matrix_alloc(n, n);

	for (int i = 0; i < n ; i++){
		for (int j = 0; j < n; j++){
			gsl_matrix_set(Z, i, j, 0);
		}
	}

	return Z;
}

gsl_matrix* build_GOE(double mean, int n, gsl_rng * r, double stdDev){
	gsl_matrix * GOE = gsl_matrix_alloc(n, n);

	//gsl_rng * r = setupRNG(nTrial);

	for (int i = 0; i < n; i++){
		for (int j = 0; j < n; j++){
			gsl_matrix_set(GOE, i, j, gsl_ran_gaussian(r, stdDev) + mean);
		}
	}
	//gsl_rng_free(r);

	modify_RealMatrix_Symmetrize(GOE, n);

	return GOE;
}

double sample_Exponential(gsl_rng * r, double mean, double n_a){
	double value;
	value = gsl_rng_uniform(r);
	value = (1 - value);
	value = log(value);
	value = -1 * mean * value;
	return value;
}
double sample_Normal(gsl_rng * r, double mean, double stdDev){
	return gsl_ran_gaussian(r, stdDev) + mean;
}
gsl_matrix * build_ESRE(double mean, int n, gsl_rng * r, double n_a) {
	if (n > 1) {
		gsl_matrix * M = gsl_matrix_alloc(n, n);
		double value;
		for (int j = 0; j < n; j++) {
			for (int i = j; i < n; i++) {
				if (i != j) {
					value = sample_Exponential(r, mean, 1);
					gsl_matrix_set(M, i, j, value);
					gsl_matrix_set(M, j, i, value);
				}
			}
			modify_RealMatrix_ColumnRateMatrix(M, n);
		}
		return M;
	}
	else {
		return NULL;
	}
}

gsl_matrix * build_ESRE_ATILDA(double mean, int n, gsl_rng * r, double n_a) {
	gsl_matrix * A = build_ESRE(mean, n, r, n_a);

	for (int i = 0; i < n; i++) {
		for (int j = i; j < n; j++) {
			if (i != j) {
				double value = gsl_matrix_get(A, i, j);
				gsl_matrix_set(A, i, j, value - mean);
				gsl_matrix_set(A, j, i, value - mean);
			}
			else {
				gsl_matrix_set(A, i, j, gsl_matrix_get(A, i, j) + ((n - 1)*(mean)));
			}
		}
	}
	return A;

}

gsl_matrix * build_RateMatrix_Markovian_GaussianSampled(double stdDev, int n, gsl_rng * r) {
	double sum;
	double value;
	gsl_matrix * W = gsl_matrix_alloc(n, n);
	for (int j = 0; j < n; j++) {
		sum = 0;
		for (int i = 0; i < n; i++) {
			if (i != j) {
				value = gsl_cdf_gaussian_P(gsl_ran_gaussian(r, stdDev), stdDev);
				gsl_matrix_set(W, i, j, value);
				sum += value;
			}
		}
		gsl_matrix_set(W, j, j, -1*sum);
	}

	return W;
}

gsl_vector * build_RealVector_Uniform(int n, gsl_rng * r) {
	gsl_vector * P = gsl_vector_alloc(n);
	double value;
	double sum = 0;
	for (int i = 0; i < n; i++) {
		value = gsl_rng_uniform(r);
		sum += value;
		gsl_vector_set(P, i, gsl_rng_uniform(r));
	}
	gsl_vector_scale(P, 1 / sum);
	return P;
}
gsl_matrix * build_RealMatrix_SymmetricComponent(gsl_matrix * W, gsl_vector * P, int n) {
	gsl_matrix * S = gsl_matrix_alloc(n, n);
	double value;

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			value = (gsl_matrix_get(W, i, j) / 2);
			value += (gsl_matrix_get(W, j, i) / 2) * (gsl_vector_get(P, i) / gsl_vector_get(P, j));
			value *= (sqrt(gsl_vector_get(P, j)) / sqrt(gsl_vector_get(P, i)));
			gsl_matrix_set(S, i, j, value);
		}
	}
	return W;
}
gsl_matrix * build_RateMatrix_Markovian_SymmetricComponent_Example(double mean, int n, gsl_rng * r, double stdDev) {
	gsl_matrix * W = build_ESRE_ATILDA(mean, n, r, 1);
	gsl_vector * P = build_RealVector_Uniform(n, r);
	gsl_matrix * S = build_RealMatrix_SymmetricComponent(W, P, n);
	return S;
}

gsl_matrix * build_RateMatrix_SinglyCycle_Exponential(double mean, int n, gsl_rng * r, double stdDev){
	
	gsl_matrix * S = gsl_matrix_alloc(n,n);
	for(int k = 0; k < n - 1; k++)
	{
		gsl_matrix_set(S, k, k+1, sample_Exponential(r, 1, 1));
	}
	gsl_matrix_set(S, n-1, 0, sample_Exponential(r, 1, 1));

	modify_RealMatrix_ColumnRateMatrix(S, n);
	return S;
	
}

gsl_matrix * generate_FromCoordArrays(matrixInfo mI, gsl_rng * r, coordArrays cA, double (*valueSampler)(gsl_rng *, double, double)){
	
	gsl_matrix * M = build_Zero(-1, mI.size, NULL, -1);
	
	for (int k = 0; k < cA.arraysSize; k++)
	{
		gsl_matrix_set(M, cA.iIndices[k], cA.jIndices[k], valueSampler(r, mI.mean, mI.stdDev));
	}
	
	return M;
}
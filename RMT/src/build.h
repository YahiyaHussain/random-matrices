#ifndef _MYBUILDER_H
#define _MYBUILDER_H

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_rng.h>


#include "structs.h"


void modify_RealMatrix_Symmetrize(gsl_matrix * M, int matrixSize);

void modify_ComplexMatrix_Symmetrize(gsl_matrix_complex * M);

void modify_RealMatrix_ColumnRateMatrix(gsl_matrix * M, int matrixSize);

void modify_RealMatrix_ColumnTransitionMatrix(gsl_matrix * M, int matrixSize);



double sample_Exponential(gsl_rng *, double, double);

double sample_Normal(gsl_rng * r, double mean, double stdDev);

gsl_matrix* build_Zero(double, int, gsl_rng *, double);

gsl_matrix* build_GOE(double, int, gsl_rng *, double);

gsl_matrix * build_ESRE(double, int, gsl_rng *, double);


gsl_matrix * build_ESRE_ATILDA(double, int, gsl_rng *, double);


gsl_matrix * build_RateMatrix_Markovian_GaussianSampled(double, int, gsl_rng *);


gsl_vector * build_SampleProbabilityVector_Uniform(int, gsl_rng *);

gsl_matrix * build_RealMatrix_SymmetricComponent(gsl_matrix *, gsl_vector *, int);

gsl_matrix * build_RateMatrix_Markovian_SymmetricComponent_Example(double, int, gsl_rng *, double);


gsl_matrix * build_RateMatrix_SinglyCycle_Exponential(double, int, gsl_rng *, double);	


gsl_matrix * generate_FromCoordArrays(matrixInfo mI, gsl_rng * r, coordArrays cA, double (*valueSampler)(gsl_rng *, double, double));
#endif
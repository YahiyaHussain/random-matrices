
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_blas.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_statistics_double.h>
#include <sys/stat.h>

#include "cycle.h"
#include "variance.h"
#include "build.h"
#include "print.h"
#include "structs.h"




gsl_matrix * convert_TripleArrays_ToRealMatrix(int matrixSize, int arraysLength, int * iIndexArray, int * jIndexArray, double * valueArray){
	gsl_matrix * Z = build_Zero(-1, matrixSize, NULL, -1);

	for(int k = 0; k < arraysLength; k++){
		gsl_matrix_set(Z, iIndexArray[k], jIndexArray[k], valueArray[k]);
	}
	return Z;
}

//expects arrays are allocated sufficiently and will return size of filled parts of array
int convert_CoordFile_ToCoordArrays(int matrixSize, int * iIndices, int * jIndices, char * fileName){
	
	char * line = NULL;
	size_t len = 0;
	ssize_t read;
	int coordArraysSize = 0;

	FILE * fp = fopen(fileName, "r");
	if (fp == NULL){
		printf("Could Not Open File");
		free(iIndices);
		free(jIndices);
		return -1;
	}
	else{
		while((read = getline(&line, &len, fp)) != -1){
			char * token = strtok(line, " ");
			iIndices[coordArraysSize] = atoi(token);

			token = strtok(NULL, " ");
			jIndices[coordArraysSize++] = atoi(token);

		}
	}
	fclose(fp);
	if (line){
		free(line);
	}
	return coordArraysSize;
}
// expects arrays are sufficiently allocated
int convert_RealMatrix_ToTripleArrays(int matrixSize, int * iIndices, int * jIndices, double * values, gsl_matrix * M){
	int numberElements = 0;
	
	for (int i = 0; i < matrixSize; i++)
	{
		for (int j = 0; j < matrixSize; j++)
		{

			if (gsl_matrix_get(M, i, j) != 0){
				numberElements++;
			}
		}
		
	}

	int kIndex = 0;
	for (int i = 0; i < matrixSize; i++)
	{
		for (int j = 0; j < matrixSize; j++)
		{

			if (gsl_matrix_get(M, i, j) != 0){
				iIndices[kIndex] = i;
				jIndices[kIndex] = j;
				values[kIndex++] = gsl_matrix_get(M, i, j);
			}
		}
		
	}
	return 0;
}
#ifndef MY_CONVERSION_H
#define MY_CONVERSION_H

gsl_matrix * convert_TripleArrays_ToRealMatrix(int matrixSize, int arraysLength, int * iIndexArray, int * jIndexArray, double * valueArray);


//expects arrays are allocated sufficiently and will return size of filled parts of array
int convert_CoordFile_ToCoordArrays(int matrixSize, int * iIndices, int * jIndices, char * fileName);

// expects arrays are sufficiently allocated
int convert_RealMatrix_ToTripleArrays(int matrixSize, int * iIndices, int * jIndices, double * values, gsl_matrix * M);
#endif


#include <string.h>
#include <stdio.h>
#include <stdlib.h>


int singlyCycle(int length){
    for(int i = 0; i < length-1; i++)
    {
        printf("%d %d\n", i+1, i);
    }
    printf("%d %d\n", 0, length - 1);
    
}
int singlyCycle_Reverse(int length){
    for(int i = 0; i < length-1; i++)
    {
        printf("%d %d\n", i, i+1);
    }
    printf("%d %d\n", length - 1, 0);

    return 0;
}

int doublyCycle(int length){
    singlyCycle(length);

    singlyCycle_Reverse(length);

    return 0;
}


int singlyCycle_WriteToFile(int length, char * FileName){
    FILE * fp;
    fp = fopen(FileName, "w+");
    for(int i = 0; i < length-1; i++)
    {
       fprintf(fp, "%d %d\n", i+1, i);
    }
    fprintf(fp, "%d %d\n", 0, length - 1);
    fclose(fp);
    return 0;
}
int singlyCycle_Reverse_AppendToFile(int length, char * FileName){
    FILE * fp;
    fp = fopen(FileName, "a+");
    for(int i = 0; i < length-1; i++)
    {
        fprintf(fp, "%d %d\n", i, i+1);
    }
    fprintf(fp, "%d %d\n", length - 1, 0);
    fclose(fp);
    return 0;
}
int singlyCycle_Reverse_WriteToFile(int length, char * FileName){
    FILE * fp;
    fp = fopen(FileName, "w+");
    for(int i = 0; i < length-1; i++)
    {
        fprintf(fp, "%d %d\n", i, i+1);
    }
    fprintf(fp, "%d %d\n", length - 1, 0);
    fclose(fp);
    return 0;
}
int doublyCycle_WriteToFile(int length, char * FileName){
    singlyCycle_WriteToFile(length, FileName);

    singlyCycle_Reverse_AppendToFile(length, FileName);

    return 0;
}

int testcycle(int argc, char * argv[]){

    if(argc > 2 && strcmp("double", argv[2])){
        doublyCycle(atoi(argv[1]));
    }
    else if(argc > 1){
        singlyCycle(atoi(argv[1]));
    }
    else
    {
        singlyCycle(5);
    }
    
    return 0;
    
}
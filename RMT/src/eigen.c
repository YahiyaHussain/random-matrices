#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_blas.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_statistics_double.h>
#include <sys/stat.h>

#include "cycle.h"
#include "variance.h"
#include "build.h"
#include "print.h"
#include "structs.h"

#include "export.h"
#include "generate.h"
#include "conversion.h"
#include "save_Eigen.h"
#include "extract.h"
#include "plot_Eigen.h"
#include "out_Eigen.h"
//_________________________________________structs________________________________
typedef void (*matrixModifier)(gsl_matrix *, int matrixSize);
typedef double (*valueSampler)(gsl_rng *, double, double);
typedef int (*CoordGenerator)(int matrixSize, char *outputFile);

double cpu_time_used;
clock_t start, end;

void startClock()
{
	start = clock();
}
void endClock_Seconds(char *jobName)
{
	printf("%s took %f seconds\n", jobName, ((double)(end - start)) / CLOCKS_PER_SEC);
}
void endClock_Minutes(char *jobName)
{
	end = clock();
	printf("%s took %f Minutes\n", jobName, ((double)(end - start)) / (CLOCKS_PER_SEC / 60));
}

char *copyCharArrayToStr(char *array, int size)
{
	char *str = malloc(sizeof(char) * (size + 1));
	for (int i = 0; i < size; i++)
	{
		str[i] = array[i];
	}
	str[size] = '\0';
	return str;
}

//_____________________________________________________________________________/
int main(int argc, char *argv[])
{

	if (argc > 2)
	{
		if (strcmp(argv[1], "timetable") == 0)
		{
			matrixModifier modifiers[1] = {modify_RealMatrix_ColumnRateMatrix};
			int numberOfModifiers = 1;
			eigenInfo eI = {1, 1};

			if (strcmp(argv[2], "singly") == 0)
			{
				for (int i = 1; i < 100; i++)
				{
					start = clock();
					matrixInfo mI = {i * 50, 1, 1};

					singlyCycle_WriteToFile(mI.size, "cycle.temp");
					save_Eigen_RealNonSymm_FromCoordFile_LogScale(mI, eI, "cycle.temp", sample_Exponential,
																  "test.txt", modifiers, numberOfModifiers, false);
					end = clock();
					cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;
					printf("%d %f\n", i * 50, cpu_time_used);
					fflush(stdout);
				}
			}
			else if (strcmp(argv[2], "doubly") == 0)
			{
				for (int i = 1; i < 100; i++)
				{
					start = clock();
					matrixInfo mI = {i * 50, 1, 1};

					doublyCycle_WriteToFile(mI.size, "cycle.temp");
					save_Eigen_RealNonSymm_FromCoordFile_LogScale(mI, eI, "cycle.temp", sample_Exponential,
																  "test.txt", modifiers, numberOfModifiers, false);

					end = clock();
					cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;
					printf("%d %f\n", i * 50, cpu_time_used);
					fflush(stdout);
				}
			}
		}
		else
		{
			//first arg is parameter file, 2nd is output
			FILE *fp = fopen(argv[1], "r");
			char *buffer;
			size_t bufsize = 32;
			char *argument;
			buffer = (char *)malloc(bufsize * sizeof(char));
			if (buffer == NULL)
			{
				perror("Unable to allocate buffer");
				exit(1);
			}

			matrixModifier modifiers[1] = {modify_RealMatrix_ColumnRateMatrix};
			int numberOfModifiers = 1;

			double mean = 1;
			double stdDev = 1;
			int matrixSize = 1;
			int numberOfTrials = 1000000;
			bool willAppendToFile = false;
			CoordGenerator CG = NULL;

			while (getline(&buffer, &bufsize, fp) != -1)
			{
				argument = strtok(buffer, " ");

				if (strncmp(argument, "size", 4) == 0)
				{
					matrixSize = atoi(strtok(NULL, " "));
					// printf("size\n");
					// printf("%d", matrixSize);
					// printf("\n");
				}

				// else if (strncmp(argument, "outputFile", 10) == 0)
				// {
				// 	outputFileName = strtok(NULL, " ");
				// 	printf("outputfile\n");
				// 	printf("%s",outputFileName);
				// 	printf("\n");
				// }

				else if (strncmp(argument, "cycle", 5) == 0)
				{
					char *type = strtok(NULL, " ");
					if (strncmp(type, "singly", 6) == 0)
					{
						CG = singlyCycle_WriteToFile;
					}
					else if (strncmp(type, "doubly", 6) == 0)
					{
						CG = doublyCycle_WriteToFile;
					}
					else
					{
						printf("unknown cycle parameter\n");
					}
				}
				else if (strncmp(argument, "trials", 6) == 0)
				{
					numberOfTrials = atoi(strtok(NULL, " "));
					// printf("numberTrials\n");
					// printf("%d", numberOfTrials);
					// printf("\n");
				}
				else if (strncmp(argument, "eigen", 17) == 0)
				{
					if (matrixSize == 1)
					{
						printf("Error: did you intitialize matrix size before numberEigenvalues?\n");
					}
					numberOfTrials = atoi(strtok(NULL, " ")) / matrixSize;
				}
				else if (strncmp(argument, "appendToFile", 12) == 0)
				{
					char *truthvalue = strtok(NULL, " ");
					if (strncmp(truthvalue, "true", 4))
					{
						willAppendToFile = true;
					}
					else if (strncmp(truthvalue, "false", 5))
					{
						willAppendToFile = false;
					}
					else
					{
						printf("unknown appendToFile parameter\n");
					}
				}
				else if (strncmp(argument, "mean", 4) == 0)
				{
					mean = atof(strtok(NULL, " "));
					// printf("mean\n");
					// printf("%f", mean);
					// printf("\n");
				}
				else if (strncmp(argument, "standardDeviation", 17) == 0)
				{
					stdDev = atof(strtok(NULL, " "));
					// printf("stddev\n");
					// printf("%f", stdDev);
					// printf("\n");
				}
			}
			matrixInfo mI = {matrixSize, mean, stdDev}; // a struct consisting of matrixSize, mean, and standard deviation

			double scaleFactor = 1 / sqrt(mI.size);

			eigenInfo eI = {scaleFactor, numberOfTrials}; // a struct consisting of scalefactor and number of trials

			if (CG != NULL)
			{

				//printf("Running with size: %d, Trials: %d, OutputFile: %s, Appending?: %d\n", matrixSize, numberOfTrials, argv[2], willAppendToFile);

				CG(mI.size, "cycle.temp");
				save_Eigen_RealNonSymm_FromCoordFile_LogScale(mI, eI, "cycle.temp", sample_Exponential, argv[2],
															modifiers, numberOfModifiers,  true);

				system("rm *.temp");
			}
			else
			{
				printf("Error: No Parameterfile/outputfile");
			}
		}
	}
	else
	{
		printf("Not enough arguments\n");
	}
	return 0;
}

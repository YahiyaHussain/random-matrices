#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_blas.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_statistics_double.h>
#include <sys/stat.h>

#include "cycle.h"
#include "variance.h"
#include "build.h"
#include "print.h"
#include "structs.h"

//_________________________________________structs________________________________
typedef void (*matrixModifier)(gsl_matrix *, int matrixSize);

typedef double (*valueSampler)(gsl_rng *, double, double);
/*_____________________________________HELPER FUNCTIONS________________________\
sets up unique rng system based on current time and # trial*/
gsl_rng * setupRNG(int nTrial);

// averages a matrix with its transpose
int comparedoubles(const void * a, const void * b);

int comparedoublesByMagnitude(const void *a, const void *b);

gsl_matrix * make_RealMatrix_ColumnRateMatrix(gsl_matrix * M, int matrixSize);

// trusts that all coords in file are in matrix
void scale_RealMatrix(gsl_matrix * M, char * filename, double k);

/*_____________________________________________________________________________|


//_____________________________________EIGEN FUNCTIONS_________________________\
//keep in mind lower triangular and diagonal of matrix are destroyed
//for some reason GSL eigen systems only detect views as square matrices*/
gsl_vector * realSymmEigen(gsl_matrix * M, int n);

gsl_vector * hermEigen(gsl_matrix_complex * M, int n);

gsl_vector_complex * realNonSymmEigen(gsl_matrix * M, int n);

//_____________________________________________________________________________|





//________________________________________import matrix functions_______________
;

// expects the file to have "i j" separated by newlines for coordinates of matrix
gsl_matrix * import_RealMatrix_FromCoordFile_Exponential(int matrixSize, char * fileName, gsl_rng * r);

gsl_matrix * import_RealMatrix_FromCoordArrays_Exponential(int matrixSize, int arraysSize, int * iIndices, int * jIndices, gsl_rng * r);

// expects the file to have "i j v" separated by newlines for coordinates and values of matrix
gsl_matrix * import_RealMatrix_FromTripleFile(int matrixSize, char * fileName);


gsl_matrix * convert_TripleArrays_ToRealMatrix(int matrixSize, int arraysLength, int * iIndexArray, int * jIndexArray, double * valueArray);


//expects arrays are allocated sufficiently and will return size of filled parts of array
int convert_CoordFile_ToCoordArrays(int matrixSize, int * iIndices, int * jIndices, char * fileName);

// expects arrays are sufficiently allocated
int convert_RealMatrix_ToTripleArrays(int matrixSize, int * iIndices, int * jIndices, double * values, gsl_matrix * M);

//_____________________________________________________________________________/

//_________________________________matrix exporting functions__________________


int export_RealMatrix_ToTripleFile(int matrixSize, char * fileName, gsl_matrix * M);

// only keepsxported file "i j "
int export_RealMatrix_ToCoordFile(int matrixSize, char * fileName, gsl_matrix * M);

//____________________________________________________________________________________/
//_____________________Test Example Functions___________________________________
gsl_matrix * test_ContinMasterEquation_ESRE(double n_a1, int n, gsl_rng * r, double n_a);


//_____________________________________________________________________________/


//________________Eigen Compiler for matrices with coords from files____________

int printEigen_RealMatrix_FromFile(gsl_matrix*(*Sample)(int, char *, gsl_rng *), char * fileName, int matrixSize, int nTrials, bool wantRealEigenValues);




//_____________________________________Eigen Compilation Functions*/

int print_Eigen_RealNonSymm(double mean, int n, double stdDev, int T, bool wantReal, gsl_matrix * (*f)(double, int, gsl_rng *, double), double scaleFactor);

int save_Eigen_RealNonSymm(double mean, int n, double stdDev, int T, gsl_matrix * (*f)(double, int, gsl_rng *, double), double scaleFactor, char * reals_File, char * imags_File);

int print_Eigen_RealNonSymm_MinusLowestMagnitudes(double mean, int n, double stdDev, int T, bool wantReal, gsl_matrix * (*f)(double, int, gsl_rng *, double), double scaleFactor);

int extractRealSymmEigen(double * arrayPointer, gsl_matrix * M, int matrixSize, double scaleFactor);

int extractRealNonSymmEigen(double * arrayPointer, gsl_matrix * M, int matrixSize, double scaleFactor, bool wantReal);

int extractRealNonSymmEigen_LogScale(double * arrayPointer, gsl_matrix * M, int matrixSize, double scaleFactor);



int print_Eigen_RealSymm(double mean, int matrixSize, double stdDev, int Trials, gsl_matrix * (*build)(double, int, gsl_rng *, double), double scaleFactor);

int write_doubleArray(double * eigenArray, int arraySize, FILE * fp);

int save_Eigen_RealSymm(double mean, int matrixSize, double stdDev, int Trials, gsl_matrix * (*build)(double, int, gsl_rng *, double), double scaleFactor, char * fileName);

int save_Eigen_RealSymm_FromCoordFile(matrixInfo mI, eigenInfo eI, char * coordFile, double (*valueSampler)(gsl_rng *, double, double), char * saveFile, void (*modifiers[])(gsl_matrix *, int matrixSize), int modifiersSize);


int save_Eigen_RealNonSymm_FromCoordFile(matrixInfo mI, eigenInfo eI, char * coordFile, double (*valueSampler)(gsl_rng *, double, double), char * saveFile, void (*modifiers[])(gsl_matrix *, int matrixSize), int modifiersSize, bool wantReal);


int save_Eigen_RealNonSymm_FromCoordFile_LogScale(matrixInfo mI, eigenInfo eI, char * coordFile, double (*valueSampler)(gsl_rng *, double, double), char * saveFile, void (*modifiers[])(gsl_matrix *, int matrixSize), int modifiersSize, bool parallel);

int save_Eigen_RealNonSymm_FromCoordFile_LogScale_K(matrixInfo mI, eigenInfo eI, 
													char * coordFile, valueSampler v, 
													char * saveFile, matrixModifier modifiers[], 
													int modifiersSize, bool parallel, scaleInfo sI);


int save_Eigen_RealSymm_FromCoordFile_parallel(matrixInfo mI, eigenInfo eI, char * coordFile, double (*valueSampler)(gsl_rng *, double, double), char * saveFile, void (*modifiers[])(gsl_matrix *, int matrixSize), int modifiersSize);


int print_Eigen_RealSymm_MinusLowestMagnitudes(double mean, int n, double stdDev, int T, gsl_matrix * (*f)(double, int, gsl_rng *, double), double scaleFactor);

int print_Spacings_RealSymm_MinusLowestMagnitudes (double mean, int n, double stdDev, int T, gsl_matrix * (*f)(double, int, gsl_rng *, double), double scaleFactor);

int print_Eigen_RealImag(double mean, int matrixSize, double stdDev, int T, gsl_matrix * (*f)(double, int, gsl_rng *, double), double n_a);


//______________________________________________________________________________

int plot_Eigen(matrixInfo mI, eigenInfo eI, histInfo hI, char * pngName, int (*coordGenerator)(int, char *));

int plot_Eigen_Complex(matrixInfo mI, eigenInfo eI, histInfo hI, char * pngName, int (*coordGenerator)(int, char *), bool isSingle);



void plot_Eigen_Complex_k(matrixInfo mI, eigenInfo eI, histInfo hI, char * pngName, int (*coordGenerator)(int, char *), bool isSingle, float k);



#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_blas.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_statistics_double.h>
#include <sys/stat.h>

#include "cycle.h"
#include "variance.h"
#include "build.h"
#include "print.h"
#include "structs.h"

int export_RealMatrix_ToTripleFile(int matrixSize, char * fileName, gsl_matrix * M){
	FILE * fp;

	fp = fopen(fileName, "w+");

	for(int i = 0; i < matrixSize; i++)
	{
		for(int j = 0; j < matrixSize; j++)
		{
			fprintf(fp, "%d %d %f\n", i, j, gsl_matrix_get(M, i, j));
		}
		
	}

	fclose(fp);
	return 0;
}
// only keepsxported file "i j "
int export_RealMatrix_ToCoordFile(int matrixSize, char * fileName, gsl_matrix * M){
	FILE * fp;

	fp = fopen(fileName, "w+");

	for(int i = 0; i < matrixSize; i++)
	{
		for(int j = 0; j < matrixSize; j++)
		{
			fprintf(fp, "%d %d\n", i, j);
		}
		
	}

	fclose(fp);
	return 0;
}
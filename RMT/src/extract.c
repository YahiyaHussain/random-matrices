
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_blas.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_statistics_double.h>
#include <sys/stat.h>

#include "cycle.h"
#include "variance.h"
#include "build.h"
#include "print.h"
#include "structs.h"

#include "export.h"
#include "generate.h"
#include "conversion.h"
#include "save_Eigen.h"
#include "extract.h"

gsl_vector * realSymmEigen(gsl_matrix * M, int n){
	gsl_matrix_view view = gsl_matrix_view_array(M->data, n,n);
	gsl_vector * eval = gsl_vector_alloc(n);
	gsl_eigen_symm_workspace * w = gsl_eigen_symm_alloc(n);
	gsl_eigen_symm(&view.matrix, eval, w);
	gsl_eigen_symm_free(w);
	return eval;
}
gsl_vector_complex * realNonSymmEigen(gsl_matrix * M, int n){
	gsl_matrix_view view = gsl_matrix_view_array(M->data, n, n);
	gsl_vector_complex * eval = gsl_vector_complex_alloc(n);
	gsl_eigen_nonsymm_workspace * w = gsl_eigen_nonsymm_alloc(n);
	gsl_eigen_nonsymm(&view.matrix, eval, w);
	gsl_eigen_nonsymm_free(w);
	return eval;
}

int extract_RealSymmEigen(double * arrayPointer, gsl_matrix * M, int matrixSize, double scaleFactor) {
	gsl_vector * eval = realSymmEigen(M, matrixSize);
	for (int j = 0; j < matrixSize; j++) {
		arrayPointer[j] = gsl_vector_get(eval, j)*(scaleFactor);
	}
	gsl_vector_free(eval);
	return 0;

}
int extract_RealNonSymmEigen(double * arrayPointer, gsl_matrix * M, int matrixSize, double scaleFactor, bool wantReal) {
	gsl_vector_complex * eval = realNonSymmEigen(M, matrixSize);
	if (wantReal)
		for (int j = 0; j < matrixSize; j++) {
			arrayPointer[j] = GSL_REAL(gsl_vector_complex_get(eval, j))*(scaleFactor);
		}
	else{
		for (int j = 0; j < matrixSize; j++) {
			arrayPointer[j] = GSL_IMAG(gsl_vector_complex_get(eval, j))*(scaleFactor);
		}
	}
	gsl_vector_complex_free(eval);
	return 0;

}
int extract_RealNonSymmEigen_LogScale(double * arrayPointer, gsl_matrix * M, int matrixSize, double scaleFactor) {
	gsl_vector_complex * eval = realNonSymmEigen(M, matrixSize);
		for (int j = 0; j < matrixSize; j++) {
			double x = GSL_REAL(gsl_vector_complex_get(eval, j))*(scaleFactor);
			double y = GSL_IMAG(gsl_vector_complex_get(eval, j))*(scaleFactor);

			double v = log(sqrt(x*x + y*y));

			arrayPointer[j] = v;
		}

	gsl_vector_complex_free(eval);
	return 0;

}
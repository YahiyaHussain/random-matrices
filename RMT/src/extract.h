#ifndef MY_EXTRACT_H
#define MY_EXTRACT_H

int extract_RealSymmEigen(double * arrayPointer, gsl_matrix * M, int matrixSize, double scaleFactor) ;

int extract_RealNonSymmEigen(double * arrayPointer, gsl_matrix * M, int matrixSize, double scaleFactor, bool wantReal) ;

int extract_RealNonSymmEigen_LogScale(double * arrayPointer, gsl_matrix * M, int matrixSize, double scaleFactor) ;
#endif
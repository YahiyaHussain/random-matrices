
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_blas.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_statistics_double.h>
#include <sys/stat.h>

#include "cycle.h"
#include "variance.h"
#include "build.h"
#include "print.h"
#include "structs.h"




// expects the file to have "i j" separated by newlines for coordinates of matrix
gsl_matrix * generate_RealMatrix_FromCoordFile_Exponential(int matrixSize, char * fileName, gsl_rng * r){

	int n_a = -1;
	gsl_matrix * M = build_Zero(n_a, matrixSize, NULL, n_a);
	int iIndex;
	int jIndex;
	double value;

	char * line = NULL;
	size_t len = 0;
	ssize_t read;

	FILE * fp = fopen(fileName, "r");
	if (fp == NULL){
		printf("Could Not Open File");
		gsl_matrix_free(M);
		return NULL;
	}
	else{
		while((read = getline(&line, &len, fp)) != -1){
			char * token = strtok(line, " ");
			iIndex = atoi(token);

			token = strtok(NULL, " ");
			jIndex = atoi(token);

			value = sample_Exponential(r, 1, 1);

			gsl_matrix_set(M, iIndex, jIndex, value);
		}
	}
	fclose(fp);
	if (line){
		free(line);
	}
	return M;
}
gsl_matrix * generate_RealMatrix_FromCoordArrays_Exponential(int matrixSize, int arraysSize, int * iIndices, int * jIndices, gsl_rng * r){
	int n_a = -1;
	gsl_matrix * M = build_Zero(n_a, matrixSize, NULL, n_a);
	
	for (int k = 0; k < arraysSize; k++)
	{
		gsl_matrix_set(M, iIndices[k], jIndices[k], sample_Exponential(r, 1, 1));
	}
	return M;
}
// expects the file to have "i j v" separated by newlines for coordinates and values of matrix
gsl_matrix * generate_RealMatrix_FromTripleFile(int matrixSize, char * fileName){
	gsl_matrix * Z = build_Zero(-1, matrixSize, NULL, -1);

	int iIndex;
	int jIndex;
	double value;

	char * line = NULL;
	size_t len = 0;
	ssize_t read;

	FILE * fp = fopen(fileName, "r");
	if (fp == NULL){
		printf("Could Not Open File");
		gsl_matrix_free(Z);
		return NULL;
	}
	else{
		while((read = getline(&line, &len, fp)) != -1){
			char * token = strtok(line, " ");
			iIndex = atoi(token);

			token = strtok(NULL, " ");
			jIndex = atoi(token);
			token = strtok(NULL, "\n");
			value = atof(token);

			gsl_matrix_set(Z, iIndex, jIndex, value);
		}
		
	}
	fclose(fp);
	if(line){
		free(line);
	}
	return Z;
}


//_____________________________________________________________________________/
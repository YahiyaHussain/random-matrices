#ifndef MY_GENERATE_H
#define MY_GENERATE_H

// expects the file to have "i j" separated by newlines for coordinates of matrix
gsl_matrix * generate_RealMatrix_FromCoordFile_Exponential(int matrixSize, char * fileName, gsl_rng * r);

gsl_matrix * generate_RealMatrix_FromCoordArrays_Exponential(int matrixSize, int arraysSize, int * iIndices, int * jIndices, gsl_rng * r);

// expects the file to have "i j v" separated by newlines for coordinates and values of matrix
gsl_matrix * generate_RealMatrix_FromTripleFile(int matrixSize, char * fileName);

#endif
//_____________________
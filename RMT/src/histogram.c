#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_histogram.h>
int main(int argc, char * argv[]){

  double plotMin = atof(argv[1]);
	double plotMax = atof(argv[2]);
	int Bins = atoi(argv[3]);
  //char * file = argv[4];


  gsl_histogram * h = gsl_histogram_alloc(Bins);
  char * line = NULL;
  size_t len = 0;
  gsl_histogram_set_ranges_uniform(h, plotMin, plotMax);

  for (int i = 4; i < argc; i++){
    FILE * fp = fopen(argv[i], "r");
    if (fp == NULL){
      printf("couldnt open file");
      return 0;
    }
    ssize_t read;
    while ((read = getline(&line, &len, fp)) != -1){
      if (line[0] == 'f'){
		  //printf("%s", line);
        break;
      }
      gsl_histogram_increment(h, atof(line));
    }

    fclose(fp);
    if (line){
      free(line);
    }
    gsl_histogram_fprintf(stdout, h, "%g", "%g");
  }

  gsl_histogram_free(h);
return 0;
}




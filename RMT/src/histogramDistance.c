#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main(int argc, char const *argv[]) {


    if (argc != 3){
        printf("ERROR: PLEASE GIVE TWO HISTOGRAMS\n");
    }




    char * line1 = NULL;
    size_t len1 = 0;

    char * line2 = NULL;
    size_t len2 = 0;

    ssize_t read1;
    ssize_t read2;

    FILE * fp1 = fopen(argv[1], "r");
    FILE * fp2 = fopen(argv[2], "r");
    //FILE * fp3 = fopen(argv[3], "a");

    double dx = 0;

    double value1 = 0;
    double value2 = 0;

    double valuesum = 0;
    int lineCount = 0;


    if (fp1 == NULL){
        printf("ERROR: COULDNT OPEN FIRST FILE\n");
    }
    if (fp2 == NULL){
        printf("ERROR: COULDNT OPEN SECOND FILE\n");
    }

    read1 = getline(&line1, &len1, fp1);
    read2 = getline(&line2, &len2, fp2);
    while( read1 != -1 && read2 != -1)
    {
        
        

        if(lineCount == 0){
            // equivalent to the last bound minus the first bound
            char * token1 = strtok(line1, " ");
            dx = -1*(atof(token1) - atof(strtok(NULL, " ")));
            value1 = atof(strtok(NULL, "\n"));
        }
        else{
            strtok(line1, " ");
            strtok(NULL, " ");
            value1 = atof(strtok(NULL, "\n"));
        }
        
        

        strtok(line2, " ");
        strtok(NULL, " ");
        value2 = atof(strtok(NULL, "\n"));
        
        valuesum += fabs(value1 - value2);


        read1 = getline(&line1, &len1, fp1);
        read2 = getline(&line2, &len2, fp2);
        lineCount++;
    }
    // printf("combined %d lines of histogram\n", lineCount);
    if (read1 != read2){
        printf("WARNING: HIST FILES HAVE DIFFERING LENGTH\n");
    }
    
    valuesum *= dx;
    valuesum /= 2;
    printf("%f\n", valuesum);

    fclose(fp1);
    fclose(fp2);
    //fclose(fp3);

    if (line1){
      free(line1);
    }
    if (line2){
      free(line2);
    }
    
    return 0;
}

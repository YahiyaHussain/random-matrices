#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[]) {

  char * line = NULL;
  size_t len = 0;

  double dx = 0;
  double value;
  int lineCount = 0;

  double maxValue = 0;
  char peakTrackerFileName[1000];
  for (int i = 1; i < argc; i+=3){
    int matrixSize = atoi(argv[i]);


	  int Trials = atoi(argv[i + 1]);
    FILE * fp = fopen(argv[i + 2], "r");
    if (fp == NULL){
      printf("couldnt open file");
      return 0;
    }

    ssize_t read;
    while ((read = getline(&line, &len, fp)) != -1){
      if (line[0] == 'f'){
        break;
      }
      char * token = strtok(line, " ");

	    if (lineCount == 0) { dx = atof(token); }
      printf("%s ", token);

	    value = atof(strtok(NULL, " "));

	    if (lineCount == 0) { dx = value - dx; }
      printf("%f ", value);

      value = atof(strtok(NULL, " t"));
      printf("%f\n", value / (matrixSize*Trials*dx));
	    lineCount++;
      if ((value / (matrixSize*Trials*dx)) > maxValue){
        maxValue = value / (matrixSize*Trials*dx);
      }
    }
    
    fclose(fp);

    sprintf(peakTrackerFileName, "peaktracker.txt");
    fp = fopen(peakTrackerFileName, "a+");
    fprintf(fp, "%d %f\n", matrixSize, maxValue);

    fclose(fp);
    if (line){
      free(line);
    }
  }
return 0;
}

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_blas.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_statistics_double.h>
#include <sys/stat.h>

#include "cycle.h"
#include "variance.h"
#include "build.h"
#include "print.h"
#include "structs.h"

#include "export.h"
#include "generate.h"
#include "conversion.h"
#include "extract.h"
typedef void (*matrixModifier)(gsl_matrix *, int matrixSize);
typedef double (*valueSampler)(gsl_rng *, double, double);



gsl_rng * OE_setupRNG(int nTrial){
	gsl_rng * r;
	const gsl_rng_type * T;
	gsl_rng_env_setup();
	T = gsl_rng_default;
	r = gsl_rng_alloc(T);
	gsl_rng_set(r, time(0) * nTrial);
	return r;
}



int out_Eigen_RealNonSymm_FromCoordFile_LogScale(matrixInfo mI, eigenInfo eI, char * coordFile, double (*valueSampler)(gsl_rng *, double, double), void (*modifiers[])(gsl_matrix *, int matrixSize), int modifiersSize){
	
	//printf("inside saveEigenCall{\n\n");
	// Assembles coordinate arrays struct storing invariant matrix data to be passed to functions
	coordArrays cA;
	// Could hypothetically need all size*size spots in matrix so need that many slots in coordinate arrays
	cA.iIndices = (int*)malloc(sizeof(int)*mI.size*mI.size);
	cA.jIndices = (int*)malloc(sizeof(int)*mI.size*mI.size);	
	// fills the coord arrays from coord file and saves their length 

	cA.arraysSize = convert_CoordFile_ToCoordArrays(mI.size, cA.iIndices, cA.jIndices, coordFile);


	// samples a matrix, calculates eigen, and stores in a file
	gsl_matrix * Sample;
	gsl_rng * r = OE_setupRNG(1);

	double eigenArray[mI.size];

	for (int i = 0; i < eI.numberTrials; i++)
	{

		Sample = generate_FromCoordArrays(mI, r, cA, valueSampler);

		for (int i = 0; i < modifiersSize; i++)
		{
			(*modifiers[i])(Sample, mI.size);
		}
		

		
		extract_RealNonSymmEigen_LogScale(eigenArray, Sample, mI.size, eI.scaleFactor);

        print_DoubleArray(eigenArray, mI.size);

		
		gsl_matrix_free(Sample);
	}

	gsl_rng_free(r);
	return 0;
}
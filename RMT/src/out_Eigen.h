#ifndef MY_OUT_EIGEN_H
#define MY_OUT_EIGEN_H


int out_Eigen_RealNonSymm_FromCoordFile_LogScale(matrixInfo mI, eigenInfo eI, char * coordFile, double (*valueSampler)(gsl_rng *, double, double), void (*modifiers[])(gsl_matrix *, int matrixSize), int modifiersSize);

#endif
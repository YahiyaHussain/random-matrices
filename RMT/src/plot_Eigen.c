#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_blas.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_statistics_double.h>
#include <sys/stat.h>

#include "cycle.h"
#include "variance.h"
#include "build.h"
#include "print.h"
#include "structs.h"

#include "export.h"
#include "generate.h"
#include "conversion.h"
#include "save_Eigen.h"
#include "extract.h"

int plot_Eigen(matrixInfo mI, eigenInfo eI, histInfo hI, char * pngName, int (*coordGenerator)(int, char *)){
	typedef void (*matrixModifier)(gsl_matrix *, int matrixSize);
	
	matrixModifier modifiers[1] = {modify_RealMatrix_ColumnRateMatrix};

	
	char norm[1000];
	char plot[1000];
	char hist[1000];
	

	char * coordfile = "cycle.temp";
	(*coordGenerator)(mI.size, coordfile);

	char * eigenfile = "eigen.temp";
	save_Eigen_RealNonSymm_FromCoordFile_LogScale(mI, eI, coordfile, sample_Exponential, eigenfile, modifiers, 1, false);
	
	system("sed '/-0.000000/d' eigen.temp > eigen.z.temp");
	system("sed '/0.000000/d' eigen.z.temp > eigen.temp");


	//char * minCommand = "cut -f1 -d\",\" eigen.temp | sort -n | head -1";
	//char * maxCommand = "cut -f1 -d\",\" eigen.temp | sort -n | tail -1";

	//sprintf(hist, "./histogram `%s` `%s` %d eigen.temp > hist.temp", minCommand, maxCommand, hI.numberOfBins);

	sprintf(hist, "./histogram %d %d %d eigen.temp > hist.temp", -45, 1, hI.numberOfBins);
	system(hist);
	//system("sed '/ 0/d' temp.hist > temp.hist.trunc");
	//system("cat temp.hist");
	sprintf(norm, "./normalize %d %d hist.temp > norm.temp", mI.size, eI.numberTrials);
	system(norm);

	sprintf(plot, "gnuplot -e \" set term png size 400, 300; set output '%s'; plot 'norm.temp' u 1:3 with lines\" ", pngName);
	system(plot);
	system("rm *.temp");
	return 0;
}
int plot_Eigen_Complex(matrixInfo mI, eigenInfo eI, histInfo hI, char * pngName, int (*coordGenerator)(int, char *), bool isSingle){
	
	
	matrixModifier modifiers[1] = {modify_RealMatrix_ColumnRateMatrix};

	
	char norm[1000];
	char hist[1000];
	


	(*coordGenerator)(mI.size, "cycle.temp");

	save_Eigen_RealNonSymm_FromCoordFile_LogScale(mI, eI, "cycle.temp", sample_Exponential, "eigen.temp", modifiers, 1, false);
	
	system("sed '/-0.000000/d' eigen.temp > eigen.z.temp");
	system("sed '/0.000000/d' eigen.z.temp > eigen.temp");


	//char * minCommand = "cut -f1 -d\",\" eigen.temp | sort -n | head -1";
	//char * maxCommand = "cut -f1 -d\",\" eigen.temp | sort -n | tail -1";

	//sprintf(hist, "./histogram `%s` `%s` %d eigen.temp > hist.temp", minCommand, maxCommand, hI.numberOfBins);
	sprintf(hist, "./histogram %f %f %d eigen.temp > hist.temp", hI.plotMin, hI.plotMax, hI.numberOfBins);
	system(hist);
	//system("sed '/ 0/d' temp.hist > temp.hist.trunc");
	//system("cat temp.hist");
	char c = '0';
	if (isSingle){
		c = 's';
	}
	else{
		c = 'd';
	}
	sprintf(norm, "./normalize %d %d hist.temp > %d%c.norm", mI.size, eI.numberTrials, mI.size, c);
	system(norm);

	//sprintf(plot, "gnuplot -e \" set term png size 400, 300; set output '%s'; plot 'norm.temp' u 1:3 with lines\" ", pngName);
	//system(plot);
	system("rm *.temp");
	return 0;


}



void plot_Eigen_Complex_k(matrixInfo mI, eigenInfo eI, histInfo hI, char * pngName, int (*coordGenerator)(int, char *), bool isSingle, float k)
{
	matrixModifier modifiers[1] = {modify_RealMatrix_ColumnRateMatrix};

	char norm[1000];
	char hist[1000];

	(*coordGenerator)(mI.size, "cycle.temp");

	
	
	singlyCycle_Reverse_WriteToFile(mI.size, "cycle.temp");

	scaleInfo sI = {1, "cycle.temp"};

	save_Eigen_RealNonSymm_FromCoordFile_LogScale_K(mI, eI, "cycle.temp", 
													sample_Exponential, "eigen.temp", 
													modifiers, 1, false, sI);

	//scale_fromCoordFile(gsl_matrix * M, char * filename, double k)

	system("sed '/-0.000000/d' eigen.temp > eigen.z.temp");
	system("sed '/0.000000/d' eigen.z.temp > eigen.temp");


	//char * minCommand = "cut -f1 -d\",\" eigen.temp | sort -n | head -1";
	//char * maxCommand = "cut -f1 -d\",\" eigen.temp | sort -n | tail -1";

	//sprintf(hist, "./histogram `%s` `%s` %d eigen.temp > hist.temp", minCommand, maxCommand, hI.numberOfBins);
	sprintf(hist, "./histogram %f %f %d eigen.temp > hist.temp", hI.plotMin, hI.plotMax, hI.numberOfBins);
	system(hist);
	//system("sed '/ 0/d' temp.hist > temp.hist.trunc");
	//system("cat temp.hist");
	char c = '0';
	if (isSingle){
		c = 's';
	}
	else{
		c = 'd';
	}
	sprintf(norm, "./normalize %d %d hist.temp > %d%c.norm", mI.size, eI.numberTrials, mI.size, c);
	system(norm);

	//sprintf(plot, "gnuplot -e \" set term png size 400, 300; set output '%s'; plot 'norm.temp' u 1:3 with lines\" ", pngName);
	//system(plot);
	system("rm *.temp");
	return;
}

#ifndef MY_PLOT_EIGEN_H
#define MY_PLOT_EIGEN_H
int plot_Eigen(matrixInfo mI, eigenInfo eI, histInfo hI, char * pngName, int (*coordGenerator)(int, char *));

int plot_Eigen_Complex(matrixInfo mI, eigenInfo eI, histInfo hI, char * pngName, int (*coordGenerator)(int, char *), bool isSingle);




void plot_Eigen_Complex_k(matrixInfo mI, eigenInfo eI, histInfo hI, char * pngName, int (*coordGenerator)(int, char *), bool isSingle, float k);
#endif
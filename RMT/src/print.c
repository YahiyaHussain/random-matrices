
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_blas.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_statistics_double.h>

int print_DoubleArray(double * array, int size) {
	for (int i = 0; i < size; i++) {
		printf("%f\n", array[i]);
	}
	return 0;
}
int print_IntArray(int * array, int size){
	for (int i = 0; i < size; i++)
	{
		printf("%d\n", array[i]);
	}
	return 0;
}
int print_RealMatrix(gsl_matrix * M, int n){
	for (int i = 0; i < n;i++){
		for (int j = 0; j < n; j++){
			printf("%f ", gsl_matrix_get(M, i, j));
		}
		printf("\n");
	}
		return 0;
}
int print_ComplexMatrix(gsl_matrix_complex * M, int n){
	for (int i = 0; i < n; i++){
		for (int j = 0; j < n; j++){
			double real = GSL_REAL(gsl_matrix_complex_get(M, i, j));
			double imag = GSL_IMAG(gsl_matrix_complex_get(M, i, j));
			printf("%f + %f(i) ", real, imag);
		}
		printf("\n");
	}
	return 0;
}
int print_RealVector(gsl_vector * V, int n){
	for (int i = 0; i < n; i++){
		printf("%f ", gsl_vector_get(V, i));
	}
	printf("\n");
	return 0;
}
int print_ComplexVector_EigenvalueEval(gsl_vector_complex * eval, int matrixSize, bool wantRealEigenValues){
	if (wantRealEigenValues){
		for(int i = 0; i < matrixSize; i++)
		{
				printf("%f\n", GSL_REAL(gsl_vector_complex_get(eval, i)));
		}
			
		}
	else
	{
		for(int i = 0; i < matrixSize; i++)
		{
			printf("%f\n", GSL_IMAG(gsl_vector_complex_get(eval, i)));
		}
	}
	return 0;
}
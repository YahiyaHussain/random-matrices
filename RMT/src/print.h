#ifndef _MYPRINT_H
#define _MYPRINT_H
int print_RealMatrix(gsl_matrix *, int);

int print_ComplexMatrix(gsl_matrix_complex *, int);

int print_RealVector(gsl_vector *, int);

int print_ComplexVector_EigenvalueEval(gsl_vector_complex *, int, bool);

int print_DoubleArray(double *, int);

int print_IntArray(int *, int);

#endif
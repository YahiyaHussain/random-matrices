#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_blas.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_statistics_double.h>
#include <sys/stat.h>

#include "cycle.h"
#include "variance.h"
#include "build.h"
#include "print.h"
#include "structs.h"

#include "export.h"
#include "generate.h"
#include "conversion.h"
#include "extract.h"
typedef void (*matrixModifier)(gsl_matrix *, int matrixSize);
typedef double (*valueSampler)(gsl_rng *, double, double);

void scale_RealMatrix_FromCoordFile(gsl_matrix * M, char * filename, double k){
	int iIndex;
	int jIndex;
	double value;

	char * line = NULL;
	size_t len = 0;
	ssize_t read;

	FILE * fp = fopen(filename, "r");
	if (fp == NULL){
		printf("Could Not Open File");
		gsl_matrix_free(M);
		return;
	}
	else{
		while((read = getline(&line, &len, fp)) != -1){
			char * token = strtok(line, " ");
			iIndex = atoi(token);

			token = strtok(NULL, " ");
			jIndex = atoi(token);

			value = gsl_matrix_get(M, iIndex, jIndex) * k;

			gsl_matrix_set(M, iIndex, jIndex, value);
		}
	}
	fclose(fp);
	if (line){
		free(line);
	}
	return;
}

int write_doubleArray(double * eigenArray, int arraySize, FILE * fp){
	for (int i = 0; i < arraySize; i++)
	{
		fprintf(fp, "%f\n", eigenArray[i]);
	}
	return 0;
	
}
gsl_rng * SG_setupRNG(int nTrial){
	gsl_rng * r;
	const gsl_rng_type * T;
	gsl_rng_env_setup();
	T = gsl_rng_default;
	r = gsl_rng_alloc(T);
	gsl_rng_set(r, time(0) * nTrial);
	return r;
}

int save_Eigen_RealSymm(double mean, int matrixSize, double stdDev, int Trials, gsl_matrix * (*build)(double, int, gsl_rng *, double), double scaleFactor, char * fileName){
	gsl_matrix * Sample;
	gsl_rng * r = SG_setupRNG(1);

	double eigenArray[matrixSize];
	FILE * fp = fopen(fileName, "w+");
	for (int i = 0; i < Trials; i++)
	{
		Sample = (*build)(mean, matrixSize, r, stdDev);
		extract_RealSymmEigen(eigenArray, Sample, matrixSize, scaleFactor);
		write_doubleArray(eigenArray, matrixSize, fp);
		gsl_matrix_free(Sample);
	}
	
	fclose(fp);
	gsl_rng_free(r);
	return 0;
}
int save_Eigen_RealSymm_FromCoordFile(matrixInfo mI, eigenInfo eI, char * coordFile, double (*valueSampler)(gsl_rng *, double, double), char * saveFile, void (*modifiers[])(gsl_matrix *, int matrixSize), int modifiersSize){
	
	//printf("inside saveEigenCall{\n\n");
	// Assembles coordinate arrays struct storing invariant matrix data to be passed to functions
	coordArrays cA;
	// Could hypothetically need all size*size spots in matrix so need that many slots in coordinate arrays
	cA.iIndices = (int*)malloc(sizeof(int)*mI.size*mI.size);
	cA.jIndices = (int*)malloc(sizeof(int)*mI.size*mI.size);	
	// fills the coord arrays from coord file and saves their length 

	//clock_t start, end;
	//double cpu_time_used;
	//start = clock();

	cA.arraysSize = convert_CoordFile_ToCoordArrays(mI.size, cA.iIndices, cA.jIndices, coordFile);

	//end = clock();
	//cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;
	//printf("Converting CoordFile To CoordArrays took %f minutes\n", cpu_time_used / 60);
	//fflush(stdout);

	// samples a matrix, calculates eigen, and stores in a file
	gsl_matrix * Sample;
	gsl_rng * r = SG_setupRNG(1);

	double eigenArray[mI.size];
	FILE * fp = fopen(saveFile, "w+");
	//double sum1 = 0, sum2 = 0, sum3 = 0, sum4 = 0;
	for (int i = 0; i < eI.numberTrials; i++)
	{
		// pass invariant matrix data through mI and cA plus a rng and valueSampler to use it
		//start = clock();
		Sample = generate_FromCoordArrays(mI, r, cA, valueSampler);
		//end = clock();
		//sum1 += ((double)(end - start)) / (CLOCKS_PER_SEC * 60);
		
		//start = clock();
		for (int i = 0; i < modifiersSize; i++)
		{
			(*modifiers[i])(Sample, mI.size);
		}
		
		//end = clock();
		//sum2 += ((double)(end - start)) / (CLOCKS_PER_SEC * 60);
		

		//start = clock();

		extract_RealSymmEigen(eigenArray, Sample, mI.size, eI.scaleFactor);
		//end = clock();
		//sum3 += ((double)(end - start)) / (CLOCKS_PER_SEC*60);
		

		//start = clock();
		write_doubleArray(eigenArray, mI.size, fp);
		//end = clock();
		//sum4 += ((double)(end - start)) / (CLOCKS_PER_SEC*60);
		
		gsl_matrix_free(Sample);
	}
	//printf("over all trials, generating matrix from coordarrays took %f minutes\n", sum1);
	//printf("over all trials, using modifiers on matrix took %f minutes\n", sum2);
	//printf("over all trials, extracting eigen from matrix took %f minutes\n", sum3);
	//printf("over all trials, writing eigen to file took %f minutes\n", sum4);
	fclose(fp);
	gsl_rng_free(r);
	printf("}\n\n");
	fflush(stdout);
	return 0;
}

int save_Eigen_RealNonSymm_FromCoordFile(matrixInfo mI, eigenInfo eI, char * coordFile, double (*valueSampler)(gsl_rng *, double, double), char * saveFile, void (*modifiers[])(gsl_matrix *, int matrixSize), int modifiersSize, bool wantReal){
	
	//printf("inside saveEigenCall{\n\n");
	// Assembles coordinate arrays struct storing invariant matrix data to be passed to functions
	coordArrays cA;
	// Could hypothetically need all size*size spots in matrix so need that many slots in coordinate arrays
	cA.iIndices = (int*)malloc(sizeof(int)*mI.size*mI.size);
	cA.jIndices = (int*)malloc(sizeof(int)*mI.size*mI.size);	
	// fills the coord arrays from coord file and saves their length 

	//clock_t start, end;
	//double cpu_time_used;
	//start = clock();

	cA.arraysSize = convert_CoordFile_ToCoordArrays(mI.size, cA.iIndices, cA.jIndices, coordFile);

	//end = clock();
	//cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;
	//printf("Converting CoordFile To CoordArrays took %f minutes\n", cpu_time_used / 60);
	//fflush(stdout);

	// samples a matrix, calculates eigen, and stores in a file
	gsl_matrix * Sample;
	gsl_rng * r = SG_setupRNG(1);

	double eigenArray[mI.size];
	FILE * fp = fopen(saveFile, "w+");
	//double sum1 = 0, sum2 = 0, sum3 = 0, sum4 = 0;
	for (int i = 0; i < eI.numberTrials; i++)
	{
		// pass invariant matrix data through mI and cA plus a rng and valueSampler to use it
		//start = clock();
		Sample = generate_FromCoordArrays(mI, r, cA, valueSampler);
		//end = clock();
		//sum1 += ((double)(end - start)) / (CLOCKS_PER_SEC * 60);
		
		//start = clock();
		for (int i = 0; i < modifiersSize; i++)
		{
			(*modifiers[i])(Sample, mI.size);
		}
		
		//end = clock();
		//sum2 += ((double)(end - start)) / (CLOCKS_PER_SEC * 60);
		

		//start = clock();
		extract_RealNonSymmEigen(eigenArray, Sample, mI.size, eI.scaleFactor, wantReal);
		//end = clock();
		//sum3 += ((double)(end - start)) / (CLOCKS_PER_SEC*60);
		

		//start = clock();
		write_doubleArray(eigenArray, mI.size, fp);
		//end = clock();
		//sum4 += ((double)(end - start)) / (CLOCKS_PER_SEC*60);
		
		gsl_matrix_free(Sample);
	}
	//printf("over all trials, generating matrix from coordarrays took %f minutes\n", sum1);
	//printf("over all trials, using modifiers on matrix took %f minutes\n", sum2);
	//printf("over all trials, extracting eigen from matrix took %f minutes\n", sum3);
	//printf("over all trials, writing eigen to file took %f minutes\n", sum4);
	fclose(fp);
	gsl_rng_free(r);
	//printf("}\n\n");
	//fflush(stdout);
	return 0;
}

int save_Eigen_RealNonSymm_FromCoordFile_LogScale(matrixInfo mI, eigenInfo eI, char * coordFile, double (*valueSampler)(gsl_rng *, double, double), char * saveFile, void (*modifiers[])(gsl_matrix *, int matrixSize), int modifiersSize, bool parallel){
	
	//printf("inside saveEigenCall{\n\n");
	// Assembles coordinate arrays struct storing invariant matrix data to be passed to functions
	coordArrays cA;
	// Could hypothetically need all size*size spots in matrix so need that many slots in coordinate arrays
	cA.iIndices = (int*)malloc(sizeof(int)*mI.size*mI.size);
	cA.jIndices = (int*)malloc(sizeof(int)*mI.size*mI.size);	
	// fills the coord arrays from coord file and saves their length 

	//clock_t start, end;
	//double cpu_time_used;
	//start = clock();

	cA.arraysSize = convert_CoordFile_ToCoordArrays(mI.size, cA.iIndices, cA.jIndices, coordFile);

	//end = clock();
	//cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;
	//printf("Converting CoordFile To CoordArrays took %f minutes\n", cpu_time_used / 60);
	//fflush(stdout);

	// samples a matrix, calculates eigen, and stores in a file
	gsl_matrix * Sample;
	gsl_rng * r = SG_setupRNG(1);

	double eigenArray[mI.size];
	FILE * fp;
	if (parallel){
		fp = fopen(saveFile, "a+");
	}
	else
	{
		fp = fopen(saveFile, "w+");
	}
	
	//double sum1 = 0, sum2 = 0, sum3 = 0, sum4 = 0;
	for (int i = 0; i < eI.numberTrials; i++)
	{
		// pass invariant matrix data through mI and cA plus a rng and valueSampler to use it
		//start = clock();
		Sample = generate_FromCoordArrays(mI, r, cA, valueSampler);
		//end = clock();
		//sum1 += ((double)(end - start)) / (CLOCKS_PER_SEC * 60);
		
		//start = clock();
		for (int i = 0; i < modifiersSize; i++)
		{
			(*modifiers[i])(Sample, mI.size);
		}
		
		//end = clock();
		//sum2 += ((double)(end - start)) / (CLOCKS_PER_SEC * 60);
		

		//start = clock();
		
		extract_RealNonSymmEigen_LogScale(eigenArray, Sample, mI.size, eI.scaleFactor);
		//end = clock();
		//sum3 += ((double)(end - start)) / (CLOCKS_PER_SEC*60);
		

		//start = clock();
		write_doubleArray(eigenArray, mI.size, fp);
		//end = clock();
		//sum4 += ((double)(end - start)) / (CLOCKS_PER_SEC*60);
		
		gsl_matrix_free(Sample);
	}
	//printf("over all trials, generating matrix from coordarrays took %f minutes\n", sum1);
	//printf("over all trials, using modifiers on matrix took %f minutes\n", sum2);
	//printf("over all trials, extracting eigen from matrix took %f minutes\n", sum3);
	//printf("over all trials, writing eigen to file took %f minutes\n", sum4);
	fclose(fp);
	gsl_rng_free(r);
	//printf("}\n\n");
	//fflush(stdout);
	return 0;
}
int save_Eigen_RealNonSymm_FromCoordFile_LogScale_K(matrixInfo mI, eigenInfo eI, 
													char * coordFile, valueSampler v, 
													char * saveFile, matrixModifier modifiers[], 
													int modifiersSize, bool parallel, scaleInfo sI){
	
	//printf("inside saveEigenCall{\n\n");
	// Assembles coordinate arrays struct storing invariant matrix data to be passed to functions
	coordArrays cA;
	// Could hypothetically need all size*size spots in matrix so need that many slots in coordinate arrays
	cA.iIndices = (int*)malloc(sizeof(int)*mI.size*mI.size);
	cA.jIndices = (int*)malloc(sizeof(int)*mI.size*mI.size);	
	// fills the coord arrays from coord file and saves their length 

	//clock_t start, end;
	//double cpu_time_used;
	//start = clock();

	cA.arraysSize = convert_CoordFile_ToCoordArrays(mI.size, cA.iIndices, cA.jIndices, coordFile);

	//end = clock();
	//cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;
	//printf("Converting CoordFile To CoordArrays took %f minutes\n", cpu_time_used / 60);
	//fflush(stdout);

	// samples a matrix, calculates eigen, and stores in a file
	gsl_matrix * Sample;
	gsl_rng * r = SG_setupRNG(1);

	double eigenArray[mI.size];
	FILE * fp;
	if (parallel){
		fp = fopen(saveFile, "a+");
	}
	else
	{
		fp = fopen(saveFile, "w+");
	}
	
	//double sum1 = 0, sum2 = 0, sum3 = 0, sum4 = 0;
	for (int i = 0; i < eI.numberTrials; i++)
	{
		// pass invariant matrix data through mI and cA plus a rng and valueSampler to use it
		//start = clock();
		Sample = generate_FromCoordArrays(mI, r, cA, v);
		//end = clock();
		//sum1 += ((double)(end - start)) / (CLOCKS_PER_SEC * 60);
		scale_RealMatrix_FromCoordFile(Sample, sI.CoordFile, sI.k);
		//start = clock();
		for (int i = 0; i < modifiersSize; i++)
		{
			(*modifiers[i])(Sample, mI.size);
		}
		
		//end = clock();
		//sum2 += ((double)(end - start)) / (CLOCKS_PER_SEC * 60);
		

		//start = clock();
		
		extract_RealNonSymmEigen_LogScale(eigenArray, Sample, mI.size, eI.scaleFactor);
		//end = clock();
		//sum3 += ((double)(end - start)) / (CLOCKS_PER_SEC*60);
		

		//start = clock();
		write_doubleArray(eigenArray, mI.size, fp);
		//end = clock();
		//sum4 += ((double)(end - start)) / (CLOCKS_PER_SEC*60);
		
		gsl_matrix_free(Sample);
	}
	//printf("over all trials, generating matrix from coordarrays took %f minutes\n", sum1);
	//printf("over all trials, using modifiers on matrix took %f minutes\n", sum2);
	//printf("over all trials, extracting eigen from matrix took %f minutes\n", sum3);
	//printf("over all trials, writing eigen to file took %f minutes\n", sum4);
	fclose(fp);
	gsl_rng_free(r);
	//printf("}\n\n");
	//fflush(stdout);
	return 0;
}

int save_Eigen_RealSymm_FromCoordFile_parallel(matrixInfo mI, eigenInfo eI, char * coordFile, double (*valueSampler)(gsl_rng *, double, double), char * saveFile, void (*modifiers[])(gsl_matrix *, int matrixSize), int modifiersSize){
	
	// Assembles coordinate arrays struct storing invariant matrix data to be passed to functions
	coordArrays cA;
	// Could hypothetically need all size*size spots in matrix so need that many slots in coordinate arrays
	cA.iIndices = (int*)malloc(sizeof(int)*mI.size*mI.size);
	cA.jIndices = (int*)malloc(sizeof(int)*mI.size*mI.size);	
	// fills the coord arrays from coord file and saves their length 

	

	cA.arraysSize = convert_CoordFile_ToCoordArrays(mI.size, cA.iIndices, cA.jIndices, coordFile);

	
	
	// samples a matrix, calculates eigen, and stores in a file
	gsl_matrix * Sample;
	gsl_rng * r = SG_setupRNG(1);

	double eigenArray[mI.size];
	FILE * fp = fopen(saveFile, "w+");
	
		// pass invariant matrix data through mI and cA plus a rng and valueSampler to use it
		Sample = generate_FromCoordArrays(mI, r, cA, valueSampler);
		
		for (int i = 0; i < modifiersSize; i++)
		{
			(*modifiers[i])(Sample, mI.size);
		}
		
		
		extract_RealSymmEigen(eigenArray, Sample, mI.size, eI.scaleFactor);

		
		write_doubleArray(eigenArray, mI.size, fp);

		gsl_matrix_free(Sample);
	

	fclose(fp);
	gsl_rng_free(r);

	fflush(stdout);
	return 0;
}

#ifndef SAVE_EIGEN_H
#define SAVE_EIGEN_H
//_________________________________________structs________________________________
typedef void (*matrixModifier)(gsl_matrix *, int matrixSize);
typedef double (*valueSampler)(gsl_rng *, double, double);
int save_Eigen_RealSymm(double mean, int matrixSize, double stdDev, int Trials, gsl_matrix * (*build)(double, int, gsl_rng *, double), double scaleFactor, char * fileName);

int save_Eigen_RealSymm_FromCoordFile(matrixInfo mI, eigenInfo eI, char * coordFile, double (*valueSampler)(gsl_rng *, double, double), char * saveFile, void (*modifiers[])(gsl_matrix *, int matrixSize), int modifiersSize);


int save_Eigen_RealNonSymm_FromCoordFile(matrixInfo mI, eigenInfo eI, char * coordFile, double (*valueSampler)(gsl_rng *, double, double), char * saveFile, void (*modifiers[])(gsl_matrix *, int matrixSize), int modifiersSize, bool wantReal);


int save_Eigen_RealNonSymm_FromCoordFile_LogScale(matrixInfo mI, eigenInfo eI, char * coordFile, double (*valueSampler)(gsl_rng *, double, double), char * saveFile, void (*modifiers[])(gsl_matrix *, int matrixSize), int modifiersSize, bool parallel);

int save_Eigen_RealNonSymm_FromCoordFile_LogScale_K(matrixInfo mI, eigenInfo eI, 
													char * coordFile, valueSampler v, 
												char * saveFile, matrixModifier modifiers[], 
													int modifiersSize, bool parallel, scaleInfo sI);


int save_Eigen_RealSymm_FromCoordFile_parallel(matrixInfo mI, eigenInfo eI, char * coordFile, double (*valueSampler)(gsl_rng *, double, double), char * saveFile, void (*modifiers[])(gsl_matrix *, int matrixSize), int modifiersSize);

#endif
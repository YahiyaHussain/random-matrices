#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char*argv[]) {
	double matrixSize = atoi(argv[1]);

	char * line = NULL;
	size_t len = 0;
	for (int i = 2; i < argc; i++) {
		FILE * fp = fopen(argv[i], "r");
		if (fp == NULL) {
			printf("couldnt open file");
			return 0;
		}
		ssize_t read;
		while ((read = getline(&line, &len, fp)) != -1) {
			if (line[0] == 'f') {
				//printf("%s", line);
				break;
			}
			printf("%f\n", atof(line) * (1.0 / sqrt(matrixSize)));
		}

		fclose(fp);
		if (line) {
			free(line);
		}
	}
}
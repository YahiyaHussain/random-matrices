#ifndef MYSTRUCTS_H
#define MYSTRUCTS_H
typedef struct{
	double scaleFactor;
	int numberTrials;
}eigenInfo;


typedef struct{
	int size;
	double mean;
	double stdDev;
} matrixInfo;

typedef struct{
	int * iIndices;
	int * jIndices;
	int arraysSize;
}coordArrays;

typedef struct{
	int * iIndices;
	int * jIndices;
	double * values;
	int arraysSize;
}tripleArrays;

typedef struct{
    double plotMin;
    double plotMax;
    int numberOfBins;
}histInfo;

typedef struct
{
	double k;
	char * CoordFile;
}scaleInfo;
#endif
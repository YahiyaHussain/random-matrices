#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_blas.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_statistics_double.h>
#include <sys/stat.h>

#include "cycle.h"
#include "variance.h"
#include "build.h"
#include "print.h"
#include "structs.h"

#include "export.h"
#include "generate.h"
#include "conversion.h"
#include "save_Eigen.h"
#include "extract.h"
#include "plot_Eigen.h"



int main(int argc, char* argv[]) {

    // char * a = "cycle1.test1";
    // char * b = "cycle2.test1";
    // char * c = "cycle3.test1";

    // singlyCycle_WriteToFile(10, a);
    // singlyCycle_WriteToFile(11, b);
    // singlyCycle_WriteToFile(11, c);

    // system("cmp %s %s", a, b);
    // system("cmp %s %s", b, c);

	return 0;
}
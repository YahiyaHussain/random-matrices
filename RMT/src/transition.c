#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
int main(int argc, char const *argv[]) {
  int matrixSize = atoi(argv[1]);
  int Trials = atoi(argv[2]);
  char * line = NULL;
  size_t len = 0;
  ssize_t read;
  FILE * fp = fopen(argv[3], "r");

  int oneCount = 0;
  int rangeCount = 0;
  if (fp == NULL){
    printf("error couldnt open file");
    return 1;
  }
  while ((read = getline(&line, &len, fp)) != -1){
    if (line[0] == 'f'){ break; }
    if (fabs(atof(line)) > 0 && fabs(atof(line)) < 1){ rangeCount++; }
    else if (strcmp(line, "1.000000")){ oneCount++; }
    else{ printf("%s\n", line); }
  }
  printf("Number of ones: %d; Number of floats [0,1]: %d\n", oneCount,rangeCount);
  if (oneCount + rangeCount == Trials * matrixSize){
    printf("Thus this is a Transition matrix.\n");
  }
  else{
    printf("Thus this is not a Transition matrix.\n");
  }
  return 0;
}

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Stack.h"
int main(int argc, char const *argv[]) {

  char * line = NULL;
  size_t len = 0;

  double dx = 0;
  double value;
  int lineCount = 0;

  struct StackNode* root = NULL;
  
  for (int i = 1; i < argc; i+=3){
    int matrixSize = atoi(argv[i]);


	  int Trials = atoi(argv[i + 1]);
    FILE * fp = fopen(argv[i + 2], "r");
    if (fp == NULL){
      printf("couldnt open file");
      return 0;
    }

    ssize_t read;
    while ((read = getline(&line, &len, fp)) != -1){
      if (line[0] == 'f'){
        break;
      }
      strtok(line, " ");

	    
	    strtok(NULL, " ");	  

      value = atof(strtok(NULL, " t"));
      
      if(value != 0.000000){
        printf(line);
        break;
      }
    }

    fclose(fp);
    if (line){
      free(line);
    }
  }
return 0;
}

/*#include <gsl/gsl_statistics_double.h>*/
/*#include <gsl/gsl_matrix.h>*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_blas.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_statistics_double.h>

gsl_rng * variance_setupRNG(int nTrial){
	gsl_rng * r;
	const gsl_rng_type * T;
	gsl_rng_env_setup();
	T = gsl_rng_default;
	r = gsl_rng_alloc(T);
	gsl_rng_set(r, time(0) * nTrial);
	return r;
}

double variance_DataArray(double data[], int stride, int size){
	return gsl_stats_variance(data, stride, size);
}

double variance_RealMatrix_OffDiag(double mean, int matrixSize, double stdDev, int n_a1, gsl_matrix * (*f)(double, int, gsl_rng *, double), double n_a2){

	gsl_rng * r = variance_setupRNG(1);

	gsl_matrix * M = (*f)(mean, matrixSize, r, stdDev);
	
	int numberOffDiagonals = matrixSize*matrixSize - matrixSize;
	double * data = (double*)malloc(numberOffDiagonals*sizeof(double));
	
	int k = 0;
	for (int i = 0; i < matrixSize; i++){
		for (int j = 0; j < matrixSize; j++){
			if (i!=j){
				data[k++] = gsl_matrix_get(M, i, j);
			}
		}
	}
	gsl_rng_free(r);
	gsl_matrix_free(M);
	return variance_DataArray(data, 1, numberOffDiagonals);
}
double variance_RealMatrix_Diag(double mean, int matrixSize, double stdDev, int n_a1, gsl_matrix * (*f)(double, int, gsl_rng *, double), double n_a2){

	gsl_rng * r = variance_setupRNG(1);

	gsl_matrix * M = (*f)(mean, matrixSize, r, stdDev);
	
	double * data = (double*)malloc(matrixSize);
	
	
	for (int i = 0; i < matrixSize; i++){
		data[i] = gsl_matrix_get(M, i, i);
	}
	gsl_rng_free(r);
	gsl_matrix_free(M);
	return variance_DataArray(data, 1, matrixSize);
}
double variance_RealMatrix(double mean, int matrixSize, double stdDev, int n_a1, gsl_matrix * (*f)(double, int, gsl_rng *, double), double n_a2){
	gsl_rng * r = variance_setupRNG(1);

	gsl_matrix * M = (*f)(mean, matrixSize, r, stdDev);

	double * data = (double *) malloc(matrixSize*matrixSize);

	for (int i = 0; i < matrixSize; i++){
		for (int j = 0; j < matrixSize; j++){
			data[i * matrixSize + j] = gsl_matrix_get(M, i, j);
		}
	}
	gsl_rng_free(r);
	gsl_matrix_free(M);
	return variance_DataArray(data, 1, matrixSize*matrixSize);
}

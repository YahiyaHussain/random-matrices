#ifndef _MYVARIANCE_H
#define _MYVARIANCE_H

double varianceOfDataArray(double, int, int );

double calculateMatrixVariance_RealOffDiagonals(double, int, double, int, gsl_matrix * (*)(double, int, gsl_rng *, double), double);

double calculateMatrixVariance_RealDiagonals(double, int, double, int, gsl_matrix * (*)(double, int, gsl_rng *, double), double);

double calculateMatrixVariance_Overall(double, int, double, int, gsl_matrix * (*)(double, int, gsl_rng *, double), double);

#endif